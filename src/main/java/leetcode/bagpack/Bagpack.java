package leetcode.bagpack;

/**
 * Backpack
 * Problem
 * Given n items with size Ai, an integer m denotes the size of a backpack. How full you can fill this backpack?
 * Example
 * If we have 4 items with size [2, 3, 5, 7], the backpack size is 11, we can select [2, 3, 5], so that the max size we can fill this backpack is 10. If the backpack size is 12. we can select [2, 3, 7] so that we can fulfill the backpack.
 * You function should return the max size we can fill in the given backpack.
 * Note
 * You can not divide any item into small pieces.
 */
public class Bagpack {

    public static int fillIter(int[] sizeArr, int capacity){
        int[][] memo = new int[sizeArr.length+1][capacity+1];
        for(int  sizeIdx =0; sizeIdx < sizeArr.length ; sizeIdx++ ){

        }
        return 0;
    }
    public static int fill(int[] size, int index, int capacity) {
        if (index == -1 || capacity == 0) {
            return 0;
        }
        // exclude item at index
        int temp1 = fill(size, index - 1, capacity);

        if (size[index] <= capacity) {
            int temp2 = size[index] + fill(size, index - 1, capacity - size[index]);
            int max = Math.max(temp1, temp2);
            return max;
        }

        return temp1;
    }

    public static int fillMemo(int[] size, int index, int capacity) {
        Integer[][] memo = new Integer[size.length+1][capacity+1];
       /* for(int i=0; i< size.length+1 ; i++){
            for (int j=0; j < capacity+1; j++){
               Integer val = memo[i][j];
               if(val == null){
                   System.out.print(" N ");
               }else{
                   System.out.print(" "+val+" ");
               }
            }
            System.out.println();
        }*/
        int ret = fillMemo(size, index, capacity, memo);

       /* System.out.println("_____________________________");
        for(int i=0; i< size.length+1 ; i++){
            for (int j=0; j < capacity+1; j++){
                Integer val = memo[i][j];
                if(val == null){
                    System.out.print(" N ");
                }else{
                    System.out.print(" "+val+" ");
                }
            }
            System.out.println();
        }*/
        return ret;
    }

    public static int fillMemo(int[] size, int index, int capacity, Integer[][] memo) {
        if (memo[index + 1][capacity] != null){
         //  System.out.println("memo is non null at index"+ index + ", capacity="+ capacity);
            return memo[index + 1][capacity];
        }
        int result = 0;
        if (index == -1 || capacity == 0) {
            result = 0;
        } else {

            int temp1 = fillMemo(size, index - 1, capacity, memo);

            if (size[index] <= capacity) {
                int temp2 = size[index] + fillMemo(size, index - 1, capacity - size[index], memo);
                result = Math.max(temp1, temp2);
            } else {
                result = temp1;
            }
        }
        // exclude item at index

        memo[index + 1][capacity] = result;

        return result;
    }


    public static void main(String[] args) {
        int[] size = {2, 3, 5, 7, 4,9,23,12,3,11,15,};
        int capacity = 93;

        int val = fill(size, size.length - 1, capacity);
        System.out.println(val);

        int valM = fillMemo(size, size.length - 1, capacity);
        System.out.println(valM);
    }
}
