package leetcode.bagpack;

/**
 * Backpack VI
 * Problem
 * Given an integer array nums with all positive numbers and no duplicates, find the number of possible combinations that add up to a positive integer target.
 * Example
 * Given nums = [1, 2, 4], target = 4
 * The possible combination ways are:
 * [1, 1, 1, 1]
 * [1, 1, 2]
 * [1, 2, 1]
 * [2, 1, 1]
 * [2, 2]
 * [4]
 * return 6
 */
public class Bagpack6 {

    public int backPackVI(int[] nums, int target) {
        if (target == 0) {
            return 1;
        }


        int count = 0;
        for (int num : nums) {
            if (num <= target) {
               count +=backPackVI(nums, target - num);
            }
        }

        return count;

    }

    public int backPackVIMemo(int[] nums, int target) {
        Integer[] memo = new Integer[target+1]; // [target +1];
        return helper(nums, target, memo);

    }

    private int helper(int[] nums, int target, Integer[] memo) {
        if(memo[target] != null) return memo[target];

        int count = 0;
        if(target == 0){
            count = 1;
        }else{
            for(int num : nums){
                if(num <= target){
                    count += helper(nums, target - num, memo);
                }
            }

        }

        memo[target] = count;
        return count;

    }

    public static void main(String[] args) {
        Bagpack6 bp6 = new Bagpack6();
        int[] nums = {1,2,4, 7, 13};
        int target = 40;
      //  int count = bp6.backPackVI(nums, target);
        int countM = bp6.backPackVIMemo(nums, target);

     //   System.out.println( count + "---- Memo="+countM);
        System.out.println( "---- Memo="+countM);
    }
}
