package leetcode.addAndSearch;


import java.util.HashMap;
import java.util.Map;

class WordDictionary {

    private TrieNode root;
    /** Initialize your data structure here. */
    public WordDictionary() {
        root = new TrieNode('_');
    }

    /** Adds a word into the data structure. */
    public void addWord(String word) {
        char[] chars = word.toCharArray();
        root.addWord(chars, 0);
    }

    /** Returns if the word is in the data structure. A word could contain the dot character '.' to represent any one letter. */
    public boolean search(String word) {
        char[] chars = word.toCharArray();
        return root.search(chars, 0);
    }

    public class TrieNode {
        char ch;
        Map<Character, TrieNode> children;
        boolean endOfWord = false;

        public TrieNode(char ch) {
            this.ch = ch;
            children = new HashMap<>();
            endOfWord = false;
        }

        public void addWord(char[] chars, int index) {
            if (index == chars.length) {
                endOfWord = true;
                return;
            }

            char ch = chars[index];
            TrieNode node = findChildNode(ch);
            node.addWord(chars, index + 1);

        }

        public boolean search(char[] chars, int index) {
            if (index == chars.length) {
                return endOfWord;
            }
            char ch = chars[index];
            if (ch == '.') {
                boolean found = false;
                for (TrieNode node : children.values()) {
                    found = found || node.search(chars, index + 1);

                }
                return found;
            }

            if (children.containsKey(ch)) {
                return children.get(ch).search(chars, index + 1);
            }

            return false;

        }

        private TrieNode findChildNode(char ch) {
            TrieNode node;
            if (children.containsKey(ch)) {
                node = children.get(ch);
            } else {
                node = new TrieNode(ch);
                children.put(ch, node);
            }
            return node;
        }

    }

    public static void main(String[] args) {
        WordDictionary dictionary = new WordDictionary();
        dictionary.addWord("bad");
        dictionary.addWord("dad");
        dictionary.addWord("mad");
        System.out.println( dictionary.search("pad")); // return false
        System.out.println( dictionary.search("bad")); // return true
        System.out.println( dictionary.search(".ad")); // return true
        System.out.println( dictionary.search("b..")); // return true
    }
}
