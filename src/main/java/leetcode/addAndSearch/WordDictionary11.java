package leetcode.addAndSearch;

public class WordDictionary11 {
     private TrieNode root;

    public WordDictionary11() {
        this.root = new TrieNode('_');
    }

    public void addWord(String word){
        char[] chars = word.toCharArray();
        _addWord(chars, 0, root);
    }

    private void _addWord(char[] chars, int charIdx, TrieNode node) {
        if(charIdx == chars.length){
            node.setEndOfWord(true);
            return;
        }
        char ch = chars[charIdx];
        TrieNode newNode = node.getChildNode(ch);
        _addWord(chars, charIdx + 1, newNode);
    }

    public boolean search(String word) {
        char[] chars = word.toCharArray();
        return root.search(chars, 0);
    }

    public static void main(String[] args) {
        WordDictionary11 dictionary = new WordDictionary11();
        dictionary.addWord("bad");
        dictionary.addWord("dad");
        dictionary.addWord("mad");
        System.out.println( dictionary.search("pad")); // return false
        System.out.println( dictionary.search("bad")); // return false
        System.out.println( dictionary.search(".ad")); // return false
        System.out.println( dictionary.search("b...")); // return false
    }


}
