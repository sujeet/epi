package leetcode.addAndSearch;

import java.util.HashMap;
import java.util.Map;

public class TrieNode {
    private char ch;
    private Map<Character, TrieNode> children;
    private boolean isEndOfWord = false;

    public TrieNode(char ch) {
        this.ch = ch;
        children = new HashMap<>();
        isEndOfWord = false;
    }

    public boolean isEndOfWord() {
        return isEndOfWord;
    }

    public void setEndOfWord(boolean endOfWord) {
        isEndOfWord = endOfWord;
    }

    public TrieNode getChildNode(char ch) {
        TrieNode node;
        if (this.children.containsKey(ch)) {
            node = this.children.get(ch);
        } else {
            node = new TrieNode(ch);
            children.put(ch, node);
        }
        return node;
    }

    public boolean search(char[] chars, int index) {
        if(index == chars.length){
            return isEndOfWord;
        }

        if(chars[index] == '.'){
            boolean found = false;
            for(TrieNode node : this.children.values()){
                found = found || node.search(chars, index+1);
            }
            return found;
        }

        if (this.children.containsKey(chars[index])) {
            return this.children.get(chars[index]).search(chars, index + 1);
        }

        return false;
    }
}
