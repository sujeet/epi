package leetcode.addNumInLinkedList;

public class AddNumber {

    class PartialSum{
        public ListNode sum = null;
        public int carry =0;
    }


    public static ListNode addListsRec(ListNode l1, ListNode l2, int carry) {
        if(l1 == null && l2== null && carry == 0){
            // stop
            return null;
        }

        int val = carry;
        if(l1 != null){
            val += l1.getVal();
        }
        if(l2 != null){
            val += l2.getVal();
        }

        int nodeVal = val %10;
        carry = val /10;
        ListNode node = new ListNode(nodeVal);

        if(l1 != null || l2 != null || carry > 0){
            ListNode more = addListsRec(
                    l1 == null ? null : l1.getNext(),
                    l2 == null ? null : l2.getNext(),
                    carry
            );
            node.setNext(more);
        }

        return node;

    }



    public static ListNode addLists(ListNode l1, ListNode l2) {
        ListNode result = null;
        ListNode tail =null;
        int carry = 0;
        int sum =0;
        while(l1 != null || l2 != null){
            sum = carry;
            if(l1 != null){
                sum += l1.getVal();
                l1 = l1.getNext();
            }
            if(l2 != null){
                sum += l2.getVal();
                l2 = l2.getNext();
            }
            if(sum > 9){
                sum = sum %10;
                carry =1;
            }
            if (result == null){
                result = new ListNode(sum);
                tail = result;
            }else{
                tail.setNext(new ListNode(sum));
                tail = tail.getNext();
            }
        }

        if(carry > 0){
            tail.setNext(new ListNode(carry));
        }

        return result;
    }

    public static void main(String[] args) {
        ListNode node9 = new ListNode(9);
        ListNode node8 = new ListNode(8, node9);
        ListNode node3 = new ListNode(3, node8);

        ListNode node7 = new ListNode(7);
        ListNode node2 = new ListNode(2, node7);

        ListNode result = addListsRec(node3, node2,0);

        node3.printRev();
        System.out.println();
        node2.printRev();
        System.out.println();

        result.printRev();

    }
}
