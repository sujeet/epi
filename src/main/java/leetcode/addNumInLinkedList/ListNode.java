package leetcode.addNumInLinkedList;

public class ListNode {
    private int val;
    private ListNode next;

    public ListNode(int val) {
       this(val, null);
    }

    public ListNode(int val, ListNode next){
        this.val = val;
        this.next = next;
    }

    public int getVal() {
        return val;
    }

    public void setVal(int val) {
        this.val = val;
    }

    public ListNode getNext() {
        return next;
    }

    public void setNext(ListNode next) {
        this.next = next;
    }

    @Override
    public String toString() {
        return "ListNode{" +
                "val=" + val +
                '}';
    }

    public void printRev(){
        if(this.getNext() == null){
            System.out.print(getVal() +"<-");
            return;
        }
        getNext().printRev();
        System.out.print(getVal() +"<-");
    }
}
