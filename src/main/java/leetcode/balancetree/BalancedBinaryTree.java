package leetcode.balancetree;

/**
 * Balanced Binary Tree
 * LintCode: https://www.lintcode.com/en/problem/balanced-binary-tree/
 * Problem
 * Given a binary tree, determine if it is height-balanced.
 * For this problem, a height-balanced binary tree is defined as a binary tree in which the depth of the two subtrees of every node never differ by more than 1.
 * Example
 * Given binary tree A={3,9,20,#,#,15,7}, B={3,#,20,15,7}
 *  A)  3            B)    3
 *     / \                  \
 *   9  20                 20
 *     /  \                / \
 *     15   7              15  7
 * The binary tree A is a height-balanced binary tree, but B is not.
 */
public class BalancedBinaryTree {

    public boolean isBalanced(TreeNode root) {
        if(root == null) return true;

        int leftHeight = 0;
        if(root.left != null){
            if(!isBalanced(root.left)){
                return false;
            }
            leftHeight = findHeight(root.left);
        }

        int rightHeight = 0;
        if(root.right != null){
           boolean isLeftBalanced =  isBalanced(root.right);
           if(!isLeftBalanced){
              return false;
           }
            rightHeight = findHeight(root.right);
        }

        return Math.abs(rightHeight - leftHeight) <= 1;
    }

    public boolean isBalanced2(TreeNode node){
        if(node == null) return true;

         int heightDiff = findHeight(node.left) -  findHeight(node.right);
         if(Math.abs(heightDiff) > 1){
             return false;
         }else{
             return isBalanced2(node.left) && isBalanced2(node.right);
         }

    }

    public boolean isBalancedBetter(TreeNode root){
        if(root == null) return true;

        if(checkHeight(root) == Integer.MIN_VALUE){
            return false;
        }
        return true;
    }

    private int checkHeight(TreeNode root) {
        if(root == null) return 0;

         int leftH = checkHeight(root.left);
         int rightH = checkHeight(root.right);

         if(Math.abs(leftH - rightH) > 1){
             return Integer.MIN_VALUE;
         }else{
             return 1+ Math.max(leftH, rightH);
         }

    }

    private int findHeight(TreeNode node) {
        if(node == null ) return 0;

        int leftHeight = findHeight(node.left);
        int rightHeight = findHeight(node.right);
        return 1 + Math.max(leftHeight, rightHeight);


    }

    public static void main(String[] args) {
        // 3,9,20,null,null,15,7
        TreeNode n3 = new TreeNode(3);
        TreeNode n9 = new TreeNode(9);
        TreeNode n20 = new TreeNode(20);
        TreeNode n15 = new TreeNode(15);
        TreeNode n7 = new TreeNode(7);

        n3.left = n9;
        n3.right = n20;
        n20.left = n15;
        n20.right = n7;


        // 1,2,2,3,3,null,null,4,4
        TreeNode m1 = new TreeNode(1);
        TreeNode m2 = new TreeNode(2);
        TreeNode m2_2 = new TreeNode(2);
        TreeNode m3 = new TreeNode(3);
        TreeNode m3_2 = new TreeNode(3);
        TreeNode m4 = new TreeNode(4);
        TreeNode m4_2 = new TreeNode(4);

        m1.left = m2;
        m1.right = m2_2;

        m2.left = m3;
        m2.right = m3_2;

        m3.left = m4;
        m3.right = m4_2;


        TreeNode t1 = new TreeNode(1);
        TreeNode t2 = new TreeNode(2);
        TreeNode t3 = new TreeNode(3);
        TreeNode t4 = new TreeNode(4);
        TreeNode t5 = new TreeNode(5);
        TreeNode t6 = new TreeNode(6);
        TreeNode t7 = new TreeNode(7);
        TreeNode t8 = new TreeNode(8);

        t1.left = t2;
        t1.right = t3;

        t2.left = t4;
        t2.right = t5;

        t4.left = t7;

        t3.right = t6;
        t6.right = t8;


        BalancedBinaryTree bbt = new BalancedBinaryTree();

        //System.out.println(  bbt.isBalanced(n3));
        //System.out.println(bbt.isBalanced(m1));

        TreeNode w1 = new TreeNode(1);
        TreeNode w2 = new TreeNode(2);
        TreeNode w3 = new TreeNode(3);

        w1.right = w2;
        w2.right = w3;
        System.out.println(bbt.isBalancedBetter(t1));



    }
}
