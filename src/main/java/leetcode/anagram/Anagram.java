package leetcode.anagram;

import java.util.*;

/**
 * Anagrams
 * Problem
 * Given an array of strings, return all groups of strings that are anagrams.
 * Example
 * Given ["lint", "intl", "inlt", "code"], return ["lint", "inlt", "intl"].
 * Given ["ab", "ba", "cd", "dc", "e"], return ["ab", "ba", "cd", "dc"].
 * Note
 * All inputs will be in lower-case
 */
public class Anagram {
    public static List<String> anagrams(String[] strs) {
        List<String> anaList = new ArrayList<>();
        List<String> result = new ArrayList<>();

        Map<String, List<String>> map = new HashMap<>();
        for (String str : strs) {
            char[] chars = str.toCharArray();
            Arrays.sort(chars);
            String sorted = String.valueOf(chars);
            if (map.containsKey(sorted)) {
                anaList.add(sorted);
                map.get(sorted).add(str);
            } else {
                ArrayList<String> list = new ArrayList<>();
                list.add(str);
                map.put(sorted, list);
            }
        }

        for (String ana : anaList) {
            result.addAll(map.get(ana));
        }
        return result;

    }

    public static void main(String[] args) {
        String[] strs = {"ab", "ba", "cd", "dc", "e"};
        List<String> ana = anagrams(strs);
        System.out.println(ana.toString());

    }
}
