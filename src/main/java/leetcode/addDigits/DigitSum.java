package leetcode.addDigits;

public class DigitSum {

    public int addDigits(int num) {
        if (num < 10) return num;

        int sum = 0;
        while(num > 10){
            sum = digiSum(num);
            num = sum;
        }
        return num;

    }

    public int digiSum(int num){

        int sum = 0;
        while(num > 0){
            sum += num%10;
            num /=10;
        }
        return sum;
    }

    public static void main(String[] args) {
        int num = 38;
        // num = 0;
        DigitSum digitSum = new DigitSum();
        System.out.println(digitSum.digiSum(num));
        System.out.println(digitSum.addDigits(num));
    }
}
