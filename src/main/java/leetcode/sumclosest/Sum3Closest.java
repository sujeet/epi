package leetcode.sumclosest;

import java.util.Arrays;

/**
 * 3Sum Closest
 * Problem
 * Given an array S of n integers, find three integers in S such that the sum is closest to a given number, target. Return the sum of the three integers.
 * Example
 * For example, given array S = {-1 2 1 -4}, and target = 1. The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).
 * Note
 * You may assume that each input would have exactly one solution.
 */
public class Sum3Closest {

    public int threeSumClosest(int[] numbers, int target) {
        Arrays.sort(numbers);
        int min = Integer.MAX_VALUE;
        int sum =0;
        for (int i = 0; i < numbers.length - 2; i++) {
           int low = i + 1;
           int high = numbers.length - 1;
            while (low < high) {
                int s = numbers[i] + numbers[low] + numbers[high];
                if(s == target){
                    return target;
                }

                if(Math.abs(s - target) < min){
                    sum = s;
                    min = Math.abs(s - target);
                }

                if(s > target){
                    // right
                    high--;
                }else{
                    low++;
                }
            }

        }
        return  sum;

    }
    public int threeSumClosest1(int[] numbers, int target) {
        Arrays.sort(numbers);

        int n = numbers.length;
        int a, b, c;
        int closestDist = Integer.MAX_VALUE;
        int closestSum = 0;
        int low, high;
        int sum;
        int dist;
        for (int i = 0; i < n - 2; i++) {
            a = numbers[i];
            low = i + 1;
            high = n - 1;
            while (low < high) {
                b = numbers[low];
                c = numbers[high];
                sum = a + b + c;
                if(sum > target){
                    // right
                    dist =  sum -target;
                    if(dist < closestDist){
                        closestDist = dist;
                        closestSum = sum;
                    }
                    high--;
                }else if(sum < target){
                    // left
                    dist = target - sum;
                    if(dist < closestDist){
                        closestDist = dist;
                        closestSum = sum;
                    }
                    low++;
                }else{
                    // found perfect match
                    return target;
                }
            }

        }
        return  closestSum;

    }

    public static void main(String[] args) {
        int[] nums = {-1, 2, 1, -4};
        int target =1;
        Sum3Closest sum3Closest = new Sum3Closest();
       int result = sum3Closest.threeSumClosest(nums, target);
        System.out.println(result);
    }
}
