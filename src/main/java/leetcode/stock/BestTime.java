package leetcode.stock;

/**
 * Best Time to Buy and Sell Stock
 * Problem
 * LeetCode 122
 * Say you have an array for which the ith element is the price of a given stock on day i.
 * If you were only permitted to complete at most one transaction (ie, buy one and sell one share of the stock), design an algorithm to find the maximum profit.
 * Example
 * Given an example [3,2,3,1,2], return 1
 */
public class BestTime {
    public int maxProfit(int[] prices) {
        int low = prices[0];
        int max = 0;
        for(int i=1; i< prices.length; i++){
            if(prices[i] > low){
                int diff = prices[i] - low;
                if(diff > max){
                    max = diff;
                }
            }else{
                low = prices[i];
            }
        }
        return max;
    }

    public int maxProfit2(int[] prices) {
        int s1 = Integer.MIN_VALUE;
        int s2=0;
        for(int price : prices){
            s1 = Math.max(s1, -price);
            s2 = Math.max(s2, s1 + price);
        }
        return s2;

    }
}
