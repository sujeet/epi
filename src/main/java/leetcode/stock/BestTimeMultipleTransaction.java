package leetcode.stock;

/**
 * Say you have an array for which the ith element is the price of a given stock on day i.
 *
 * Design an algorithm to find the maximum profit. You may complete as many transactions as you like (i.e., buy one and sell one share of the stock multiple times).
 *
 * Note: You may not engage in multiple transactions at the same time (i.e., you must sell the stock before you buy again).
 *
 * Example 1:
 *
 * Input: [7,1,5,3,6,4]
 * Output: 7
 * Explanation: Buy on day 2 (price = 1) and sell on day 3 (price = 5), profit = 5-1 = 4.
 *              Then buy on day 4 (price = 3) and sell on day 5 (price = 6), profit = 6-3 = 3.
 * Example 2:
 *
 * Input: [1,2,3,4,5]
 * Output: 4
 * Explanation: Buy on day 1 (price = 1) and sell on day 5 (price = 5), profit = 5-1 = 4.
 *              Note that you cannot buy on day 1, buy on day 2 and sell them later, as you are
 *              engaging multiple transactions at the same time. You must sell before buying again.
 * Example 3:
 *
 * Input: [7,6,4,3,1]
 * Output: 0
 * Explanation: In this case, no transaction is done, i.e. max profit = 0.
 *
 * see: https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/solution/
 */
public class BestTimeMultipleTransaction {
    public int maxProfit(int[] prices) {
        int max = 0;
        for(int i=0; i< prices.length-1; i++){
            if(prices[i+1] > prices[i]) max += prices[i+1] - prices[i];
        }
        return max;
    }


    public int calculate(int prices[], int idx) {
        if(idx >= prices.length) return 0;

        int max = 0;
        for(int i = idx; i< prices.length; i++){
            int maxProfit = 0;
            for(int j = i+1; j< prices.length; j++){
                if(prices[i] < prices[j]){
                    int profit = (prices[j] - prices[i]) + calculate(prices, j+1);
                    if(profit > maxProfit){
                        maxProfit = profit;
                    }
                }
            }
            if(max < maxProfit)
              max = maxProfit;
        }
        return max;
    }

    public int calculate2(int prices[], int s) {
        if (s >= prices.length)
            return 0;
        int max = 0;
        for (int start = s; start < prices.length; start++) {
            int maxprofit = 0;
            for (int i = start + 1; i < prices.length; i++) {
                if (prices[start] < prices[i]) {
                    int profit = calculate(prices, i + 1) + prices[i] - prices[start];
                    if (profit > maxprofit)
                        maxprofit = profit;
                }
            }
            if (maxprofit > max)
                max = maxprofit;
        }
        return max;
    }

    public static void main(String[] args) {
        int[] prices = {7,1,5,3,6,4};
        BestTimeMultipleTransaction b = new BestTimeMultipleTransaction();
        System.out.println(b.maxProfit(prices));
        System.out.println(b.calculate(prices, 0));
    }
}
