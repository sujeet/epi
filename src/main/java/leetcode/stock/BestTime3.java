package leetcode.stock;
import  static  java.lang.Math.*;

/**
 * Best Time to Buy and Sell Stock III
 * Problem
 * LeetCode 123
 * LintCode 151
 * Say you have an array for which the ith element is the price of a given stock on day i.
 * Design an algorithm to find the maximum profit. You may complete at most two transactions.
 * Notice
 * You may not engage in multiple transactions at the same time (ie, you must sell the stock before you buy again).
 * Example
 * Given an example [4,4,6,1,1,4,2,5], return 6.
 *
 */
public class BestTime3 {
    public int maxProfit(int[] prices) {
        int start = 0;
        int end = prices.length;
        int maxProfit = maxProfit(prices, start, end);


        for(int pivot = end -2 ; pivot >1; pivot--){
            int profit = maxProfit(prices, start, pivot) + maxProfit(prices, pivot, end);
            if(profit > maxProfit){
                maxProfit = profit;
            }

        }
        return maxProfit;

    }



    public int maxProfit(int[] prices, int start, int end){

        int low = prices[start];
        int max = 0;
        for(int i=start+1; i<end; i++){
            if(prices[i] < low){
                low = prices[i];
            }else{
                int profit = prices[i] - low;
                if(profit > max){
                    max = profit;
                }
            }
        }
       System.out.println("Max profit from "+ start + " -> " + end + ", max="+ max);
        return max;
    }

    int maxProfit2(int[] prices) {
        if(prices.length == 0) return 0;
        int s1=Integer.MIN_VALUE,s2=0,s3=Integer.MIN_VALUE,s4=0;

       // System.out.format("s1=%d, s2=%d, s3=%d, s4=%d  for price=\n", s1, s2, s3, s4, prices[0]);

        for(int i=0;i<prices.length;++i) {
            s1 = max(s1, -prices[i]);
            s2 = max(s2, s1+prices[i]);
            s3 = max(s3, s2-prices[i]);
            s4 = max(s4, s3+prices[i]);
            System.out.format("s1=%d, s2=%d, s3=%d, s4=%d  for price=%d\n", s1, s2, s3, s4, prices[i]);
        }
        return max(0,s4);
    }

    public int maxProfit3(int[] prices) {
        int buy1 = Integer.MIN_VALUE, sell1 = 0,
                buy2 = Integer.MIN_VALUE, sell2 = 0;

        for (int price : prices) {
            sell2 = Math.max(sell2, price + buy2);
            buy2 = Math.max(buy2, sell1 - price);
            sell1 = Math.max(sell1, price + buy1);
            buy1 = Math.max(buy1, -price);
            System.out.format("b1=%d, s1=%d ,  b2=%d, s2=%d at price=%d \n", buy1, sell1, buy2, sell2, price);

        }
        return sell2;
    }


    public static void main(String[] args) {
        BestTime3 bt3 = new BestTime3();
        int[] prices1 = {10,4,4,6,1,1,4,2,5};
        int ans1 = 6;

        System.out.println(bt3.maxProfit2(prices1) + "---- needed="+ans1 );

     /*   int[] prices2 = {3,3,5,0,0,3,1,4};
        int ans2 = 6;

       System.out.println(bt3.maxProfit2(prices2) + "---- needed="+ans2 );

        int[] prices3 = {1,2,3,4,5};
        int ans3 = 4;

        System.out.println(bt3.maxProfit2(prices3) + "---- needed="+ans3 );

        int[] prices4 = {1,4,2,7};
        int ans4 = 8;

        System.out.println(bt3.maxProfit2(prices4) + "---- needed="+ans4 );*/


    }
}
