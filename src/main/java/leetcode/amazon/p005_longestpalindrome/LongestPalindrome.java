package leetcode.amazon.p005_longestpalindrome;

/**
 * Given a string s, find the longest palindromic substring in s. You may assume that the maximum length of s is 1000.
 * <p>
 * Example 1:
 * <p>
 * Input: "babad"
 * Output: "bab"
 * Note: "aba" is also a valid answer.
 * Example 2:
 * <p>
 * Input: "cbbd"
 * Output: "bb"
 */
public class LongestPalindrome {
    public String longestPalindrome(String s) {
        if(s == null || s.length() < 2) return s;
        int length = s.length();
        int left=0;
        int right=0;
        for(int i=0; i< length; i++){
            int l1 = expandAroundCenter(s, i, i);
            int l2 = expandAroundCenter(s, i, i+1);
            int len = Math.max(l1, l2);
            if(len > right - left){
                left = i - (len -1)/2;
                right = i+ len/2;
            }
        }
        return s.substring(left, right+1);
    }

    private int expandAroundCenter(String s, int left, int right) {
        int length = s.length();
        if( left >=0 && right < length && s.charAt(left) == s.charAt(right)){
            left--;
            right++;
        }
        return right - left -1;
    }

    public String longestPalindromeDP(String s) {
        if(s == null) return s;
        int length = s.length();
        if(length < 2) return s;

        int left=0;
        int right=0;
        boolean[][] isPalindrome = new boolean[length][length];

        for(int size=1; size <= length ; size++){
            for(int i=0; i <= length - size; i++){
                int start = i;
                int end = start + size -1;
                System.out.println("Size="+size +", start="+start + ", end="+end + ", word="+ s + ", len="+length);
                if(start == end){
                    isPalindrome[start][end] = true;
                }else{
                    boolean isInnerWordPalindrome = isPalindrome[start+1][end-1] || (end -start <=2);
                    if(s.charAt(start) == s.charAt(end) && isInnerWordPalindrome){

                            isPalindrome[start][end] = true;
                            if(end - start > right - left){
                                left = start;
                                right = end;
                            }
                        System.out.println("word="+ s.substring(start, end+1) + " is Palindrome");

                    }else{
                        isPalindrome[start][end] = false;
                        System.out.println("word="+ s.substring(start, end+1) + " is NOT Palindrome");
                    }
                }


            }
        }
        return s.substring(left, right+1);

    }

    public String longestPalindrome22(String s) {
        if (s == null || s.isEmpty()) return "";

        char[] chars = s.toCharArray();
        int len = s.length();
        String palin = s.substring(0,1);
        for(int i = 0; i< len ; i++){
            String palinAtI = findPalin(s, i);
            if(palin.length() < palinAtI.length()){
                palin = palinAtI;
            }
            if(i >= len - palin.length()){
                break;
            }
        }
        return palin;
    }

    private String findPalin(String s, int idx) {
        char ch = s.charAt(idx);
        for(int i = s.length() -1; i > idx ; i--){
            if (ch == s.charAt(i)){
                if(isPalin(s, idx, i)){
                    return s.substring(idx, i+1);
                }
            }
        }
        return s.substring(idx, idx+1);
    }

    private boolean isPalin(String s, int start, int end){
        while(start < end){
            if(s.charAt(start) != s.charAt(end)){
                return false;
            }
            start++;
            end--;
        }
        return true;
    }

    public String longestPalindrome2(String s) {
        int[] maxStart = new int[1], maxEnd = new int[1]; // use array in order to pass by reference instead of pass by value

        for (int i = 0; i < s.length()-1; i++) {
            extend(s, i, i, maxStart, maxEnd);
            extend(s, i, i+1, maxStart, maxEnd);
        }

        return s.substring(maxStart[0], maxEnd[0]+1);
    }

    private void extend(String s, int i, int j, int[] maxStart, int[] maxEnd) {
        // loop until meet invalid match
        while (i >= 0 && j < s.length() && s.charAt(i) == s.charAt(j)) {
            i--; j++;
        }

        i++; j--; // back to the last valid match

        if (j - i + 1 > maxEnd[0] - maxStart[0] + 1) {
            maxStart[0] = i;
            maxEnd[0] = j;
        }
        

    }


    public static void main(String[] args) {
    //    String s = "abadab";
        String s = "cbbd";
        LongestPalindrome lp = new LongestPalindrome();
        System.out.println(lp.longestPalindrome(s));

    }
}
