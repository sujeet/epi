package leetcode.amazon.p017_letter_phone;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Given a string containing digits from 2-9 inclusive, return all possible letter combinations that the number could represent.
 *
 * A mapping of digit to letters (just like on the telephone buttons) is given below. Note that 1 does not map to any letters.
 *   1 - none
 *   2 - abc
 *   3 - def
 *   4 - ghi
 *   5 - jkl
 *   6 - mno
 *   7 - pqrs
 *   8 - tuv
 *   9 - wzyz
 *   0 
 *
 *
 * Example:
 *
 * Input: "23"
 * Output: ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"].
 * Note:
 *
 * Although the above answer is in lexicographical order, your answer could be in any order you want.
 */
public class PhoneLetter {
    public List<String> letterCombinations(String digits) {
        Map<Character, List<Character>> letterMap = new HashMap<>();
        letterMap.put('2', Arrays.asList('a','b','c'));
        letterMap.put('3', Arrays.asList('d','e','f'));
        letterMap.put('4', Arrays.asList('g','h','i'));
        letterMap.put('5', Arrays.asList('j','k','l'));
        letterMap.put('6', Arrays.asList('m','n','o'));
        letterMap.put('7', Arrays.asList('p','q','r','s'));
        letterMap.put('8', Arrays.asList('t','u','v'));
        letterMap.put('9', Arrays.asList('w','x','y','z'));

        List<String> result  = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        helper(digits, sb, letterMap, result);
        return result;
    }

    private void helper(String digits, StringBuilder sb, Map<Character, List<Character>> letterMap, List<String> result) {
        if(digits.length() == sb.length()){
            result.add(sb.toString());
            return;
        }
        for(char ch : letterMap.get(digits.charAt(sb.length()))){
            sb.append(ch);
            helper(digits, sb, letterMap, result);
            sb.deleteCharAt(sb.length() -1);
        }
    }

    public List<String> letterCombinationsItr(String digits) {
        Map<Character, List<Character>> letterMap = new HashMap<>();
        letterMap.put('2', Arrays.asList('a','b','c'));
        letterMap.put('3', Arrays.asList('d','e','f'));
        letterMap.put('4', Arrays.asList('g','h','i'));
        letterMap.put('5', Arrays.asList('j','k','l'));
        letterMap.put('6', Arrays.asList('m','n','o'));
        letterMap.put('7', Arrays.asList('p','q','r','s'));
        letterMap.put('8', Arrays.asList('t','u','v'));
        letterMap.put('9', Arrays.asList('w','x','y','z'));
        List<List<String>> input = new ArrayList<>();
        for(int i=0; i<digits.length(); i++){
            char ch = digits.charAt(i);
            if(letterMap.containsKey(ch)){
                input.add(letterMap.get(ch).stream().map(String::valueOf).collect(Collectors.toList()));
            }
        }
        while(input.size() > 1){
          //  System.out.println("while input="+input);
            List<String> first = input.remove(0);
            List<String> second = input.remove(0);
            List<String>  merged = merge(first, second);
            input.add(0, merged);

        }
       return input.get(0);
    }

    private List<String> merge(List<String> first, List<String> second) {
        List<String> result = new ArrayList<>();
        for(String fst : first){
            for(String sec: second){
                result.add(fst + sec);
            }
        }
        return result;
    }


    public static void main(String[] args) {
        PhoneLetter pl = new PhoneLetter();
        List<String>  out = pl.letterCombinations("233");
        System.out.println(out);
    }
}
