package leetcode.amazon.p020_valid_parenthesis;

import java.sql.SQLOutput;
import java.util.Stack;

public class ValidParenthesis {
    public boolean isValid(String s) {
        Stack<Character> stack = new Stack<Character>();
        for (char c : s.toCharArray()) {
            if (c == '(')
                stack.push(')');
            else if (c == '{')
                stack.push('}');
            else if (c == '[')
                stack.push(']');
            else if (stack.isEmpty() || stack.pop() != c)
                return false;
        }
        return stack.isEmpty();
    }
    public boolean isValid2(String s) {
        Stack<Character> stack = new Stack<>();
        for(int i=0; i< s.length(); i++){
            char ch = s.charAt(i);
            if(ch == '(' || ch == '{' || ch == '['){
                stack.push(ch);
            }else if(ch == ')'){
                if(stack.isEmpty() || stack.pop() != '('){
                    return false;
                }
            }else if(ch == '}'){
                if(stack.isEmpty() || stack.pop() != '{'){
                    return false;
                }
            }else if(ch == ']'){
                if(stack.isEmpty() || stack.pop() != '['){
                    return false;
                }
            }
        }
        return  stack.isEmpty();
    }
    public static void main(String[] args) {
        Stack<Character> st = new Stack<>();
        System.out.println(st.peek());
    }
}
