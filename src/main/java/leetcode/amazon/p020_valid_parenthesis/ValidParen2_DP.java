package leetcode.amazon.p020_valid_parenthesis;

import java.util.Stack;

/**
 * Given a string containing only three types of characters: '(', ')' and '*', write a function to check whether this string is valid. We define the validity of a string by these rules:
 * <p>
 * Any left parenthesis '(' must have a corresponding right parenthesis ')'.
 * Any right parenthesis ')' must have a corresponding left parenthesis '('.
 * Left parenthesis '(' must go before the corresponding right parenthesis ')'.
 * '*' could be treated as a single right parenthesis ')' or a single left parenthesis '(' or an empty string.
 * An empty string is also valid.
 * Example 1:
 * Input: "()"
 * Output: True
 * Example 2:
 * Input: "(*)"
 * Output: True
 * Example 3:
 * Input: "(*))"
 * Output: True
 */
public class ValidParen2_DP {
    /**
     * https://leetcode.com/problems/valid-parenthesis-string/discuss/107577/Short-Java-O(n)-time-O(1)-space-one-pass
     *
     * @param s
     * @return
     */
    public boolean checkValidString(String s) {
        int length = s.length();
        if (length == 0) return true;

        boolean[][] dp = new boolean[length][length];
        int from;
        int to;
        // Handle size 1 and size 2
        for (int i = 0; i < length; i++) {
            // case size = 1 diagonal
            if (s.charAt(i) == '*') dp[i][i] = true;
            //case size =2 , second diagonal
            if (i < length - 1 &&
                    (s.charAt(i) == '(' || s.charAt(i) == '*') &&
                    (s.charAt(i + 1) == ')' || s.charAt(i + 1) == '*')) {
                dp[i][i + 1] = true;
            }
        }

       // print(dp, s);

        for (int size = 3; size <= length; size++) {
            for (int i = 0; i < length - size + 1; i++) {
                from = i;
                to = i + size + -1;
              //  System.out.println("size="+ size + ", from="+ from + ", to="+to);
                char ch = s.charAt(from);
                if (ch == '*' && dp[from + 1][to]) {
                    dp[from][to] = true;
                } else if (ch == '*' || ch == '(') {
                    // can we find k such that from to k-1 is valid and k+1 to to is valid
                    for (int k = from + 1; k <= to; k++) {
                        char chAtK = s.charAt(k);
                        if (chAtK == '*' || chAtK == ')') {

                            //  (k == i+1 || dp[i+1][k-1]) &&
                            //                                (k == i+size || dp[k+1][i+size])
                            if ( (k == from+ 1 || dp[from+1][k - 1]) && (to == k  || dp[k + 1][to])) {
                                dp[from][to] = true;
                            }

                        }
                    }
                }
            }
        }

        return dp[0][length - 1];

    }

    private void print(boolean[][] dp, String s) {
        System.out.println("Print dp for s="+ s);
        for (int i = 0; i < dp.length; i++) {
            for (int j = 0; j < dp.length; j++) {
                System.out.print(dp[i][j] ? " 1 " : " 0 ");
            }
            System.out.println();
        }
    }


    public static void main(String[] args) {
        ValidParen2_DP vp = new ValidParen2_DP();
      //  boolean valid = vp.checkValidString("()");
     //   System.out.println(valid + " string=" + "()");

       // System.out.println(vp.checkValidString("(*)"));
       // System.out.println(vp.checkValidString("(*))"));
        System.out.println(vp.checkValidString("((*)")); //
       // System.out.println(vp.checkValidString(")("));
       System.out.println(vp.checkValidString("(((***))")); //
       // System.out.println(vp.checkValidString("(((***))***))))))))"));
    }
}
