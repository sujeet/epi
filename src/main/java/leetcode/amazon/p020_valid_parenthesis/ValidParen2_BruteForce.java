package leetcode.amazon.p020_valid_parenthesis;

public class ValidParen2_BruteForce {
    public boolean checkValidString(String s) {
        boolean[] ans = new boolean[1];
        solve(new StringBuilder(s), 0, ans);
        return ans[0];
    }

    private void solve(StringBuilder sb, int index, boolean[] ans) {
        if(index == sb.length()){
            ans[0] = ans[0] | valid(sb);
        }else if(sb.charAt(index) == '*'){
            sb.setCharAt(index, '(');
            solve(sb, index, ans);
            if (ans[0]) return;

            sb.setCharAt(index, ')');
            solve(sb, index, ans);
            if (ans[0]) return;

            sb.setCharAt(index, ' ');
            solve(sb, index, ans);
            if (ans[0]) return;

        }else{
            solve(sb, index+1, ans);
        }
    }

    private boolean valid(StringBuilder sb) {
        int balance = 0;
        for(int i=0; i< sb.length(); i++){
            char ch = sb.charAt(i);
            if(ch == '('){
                balance +=1;
            }else if(ch == ')'){
                balance -=1;
                if(balance < 0){
                    return false;
                }
            }
        }
        return balance == 0;
    }

    public static void main(String[] args) {
        ValidParen2_BruteForce vp = new ValidParen2_BruteForce();
        System.out.println(vp.checkValidString("(*)"));
        System.out.println(vp.checkValidString("(*))"));
        System.out.println(vp.checkValidString("((*)"));
        System.out.println(vp.checkValidString(")("));
        System.out.println(vp.checkValidString("(((***))"));
        System.out.println(vp.checkValidString("(((***))***))))))))"));
    }

}
