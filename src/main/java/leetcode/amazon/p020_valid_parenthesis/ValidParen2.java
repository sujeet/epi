package leetcode.amazon.p020_valid_parenthesis;

import java.util.Stack;

/**
 * Given a string containing only three types of characters: '(', ')' and '*', write a function to check whether this string is valid. We define the validity of a string by these rules:
 *
 * Any left parenthesis '(' must have a corresponding right parenthesis ')'.
 * Any right parenthesis ')' must have a corresponding left parenthesis '('.
 * Left parenthesis '(' must go before the corresponding right parenthesis ')'.
 * '*' could be treated as a single right parenthesis ')' or a single left parenthesis '(' or an empty string.
 * An empty string is also valid.
 * Example 1:
 * Input: "()"
 * Output: True
 * Example 2:
 * Input: "(*)"
 * Output: True
 * Example 3:
 * Input: "(*))"
 * Output: True
 */
public class ValidParen2 {
    /**
     * https://leetcode.com/problems/valid-parenthesis-string/discuss/107577/Short-Java-O(n)-time-O(1)-space-one-pass
     * @param s
     * @return
     */
    public boolean checkValidString(String s) {
        // https://leetcode.com/problems/valid-parenthesis-string/discuss/107577/Short-Java-O(n)-time-O(1)-space-one-pass
        int lowLeftParenthesisCount = 0;
        int highLeftParenthesisCount = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(') {
                lowLeftParenthesisCount++;
                highLeftParenthesisCount++;
            } else if (s.charAt(i) == ')') {
                if (lowLeftParenthesisCount > 0) {
                    lowLeftParenthesisCount--;
                }
                highLeftParenthesisCount--;
            } else {
                if (lowLeftParenthesisCount > 0) {
                    lowLeftParenthesisCount--;
                }
                highLeftParenthesisCount++;
            }
            if (highLeftParenthesisCount < 0) {
                return false;
            }
        }
        return lowLeftParenthesisCount == 0;
    }

    public boolean checkValidString2(String s) {
        if(s == null || s.isEmpty()) return true;
        Stack<Character> st = new Stack<>();
        for(int i=0; i< s.length(); i++){
            char ch = s.charAt(i);
            if(ch == '(' || ch == '*'){
                st.push(ch);
            }else if(ch == ')'){
                if(st.isEmpty()){
                    return false;
                }
                char stch = st.pop();
                if(!(stch == '(' || stch =='*')){
                    return false;
                }
            }
        }

        if(st.isEmpty()){
            return true;
        }

        while(!st.isEmpty()){
            char stch = st.pop();
            if(stch == '*'){
                if(st.isEmpty()){
                    return false;
                }else{
                    char stch2 = st.pop();
                    if(stch2 != '('){
                        return false;
                    }
                }
            }else{
                return false;
            }
        }

        return true;

    }

    public static void main(String[] args) {
        ValidParen2 vp2 = new ValidParen2();
         boolean  valid = vp2.checkValidString(")");
        System.out.println(valid + " string="+ "()");
    }
}
