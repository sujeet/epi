package leetcode.amazon.p078_subsets;

import java.util.ArrayList;
import java.util.List;

/**
 * https://leetcode.com/problems/subsets/description/
 * Given a set of distinct integers, nums, return all possible subsets (the power set).
 *
 * Note: The solution set must not contain duplicate subsets.
 *
 * Example:
 *
 * Input: nums = [1,2,3]
 * Output:
 * [
 *   [3],
 *   [1],
 *   [2],
 *   [1,2,3],
 *   [1,3],
 *   [2,3],
 *   [1,2],
 *   []
 * ]
 */
public class Subset {
    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> results = new ArrayList<>();
        List<Integer> res = new ArrayList<>();
        helper(nums, 0, res, results);
        return results;
    }

    private void helper(int[] nums, int index, List<Integer> result,  List<List<Integer>> results) {
        if(nums.length == index){
            results.add(new ArrayList<>(result));
            return;
        }

        int num = nums[index];
        // do not include
        helper(nums, index+1, result, results);
        // include
        result.add(num);
        helper(nums, index+1, result, results);
        result.remove(result.size() -1);
    }

    public static void main(String[] args) {
        int[] nums = {1,2,3};
        Subset subset = new Subset();
        List<List<Integer>> res = subset.subsets(nums);
        System.out.println(res);
    }
}
