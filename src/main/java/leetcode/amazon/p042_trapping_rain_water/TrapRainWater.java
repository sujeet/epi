package leetcode.amazon.p042_trapping_rain_water;

import java.util.ArrayList;
import java.util.List;

/**
 * https://leetcode.com/problems/trapping-rain-water/solution/
 * https://leetcode.com/problems/trapping-rain-water/description/
 * 42. Trapping Rain Water
 *
 * Given n non-negative integers representing an elevation map where the width of each bar is 1, compute how much water it is able to trap after raining.
 *
 *
 * The above elevation map is represented by array [0,1,0,2,1,0,1,3,2,1,2,1]. In this case, 6 units of rain water (blue section) are being trapped. Thanks Marcos for contributing this image!
 *
 * Example:
 *
 * Input: [0,1,0,2,1,0,1,3,2,1,2,1]
 * Output: 6
 */
public class TrapRainWater {
    public int trap(int[] height) {
        if(height == null) return  0;

        int length = height.length;
        if(length == 0) return 0;

        int[] leftHeighest = new int[length];
        leftHeighest[0] = height[0];


        for(int i = 1; i< length; i++){
            leftHeighest[i] = Math.max(leftHeighest[i-1], height[i]);
        }

        int[] rightHeighest = new int[length];
        rightHeighest[length -1] = height[length -1];
        int waterAmount =0;
        for(int i= length -2; i >=0; i--){
            rightHeighest[i] = Math.max(rightHeighest[i+1], height[i]);
            waterAmount += Math.min(leftHeighest[i], rightHeighest[i]) - height[i];
        }
        return waterAmount;
    }



    public static void main(String[] args) {
        TrapRainWater rainWater = new TrapRainWater();
        int[] heights = {0,1,0,2,1,0,1,3,2,1,2,1};
        int expected = 6;
        int result = rainWater.trap(heights);
        System.out.println("Got="+ result + ", Expected="+expected);
    }
}
