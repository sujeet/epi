package leetcode.amazon.p049_group_anagram;

import java.util.*;

public class GroupAnangram {
    public List<List<String>> groupAnagrams(String[] strs) {
        List<List<String>> results = new ArrayList<>();
        if(strs == null || strs.length == 0){
            return results;
        }

        Map<String, List<String>> map = new HashMap<>();
        for(String str : strs){
            char[] chars = str.toCharArray();
            Arrays.sort(chars);
            String key = String.valueOf(chars);
            if(map.containsKey(key)){
                map.get(key).add(str);
            }else{
                List<String> res = new ArrayList<>();
                res.add(str);
                map.put(key, res);
            }
        }
        return new ArrayList(map.values());

    }
}
