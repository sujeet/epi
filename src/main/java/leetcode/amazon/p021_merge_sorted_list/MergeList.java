package leetcode.amazon.p021_merge_sorted_list;

import leetcode.amazon.ListNode;

public class MergeList {
    public ListNode mergeTwoListsRec(ListNode l1, ListNode l2){
        if(l1 == null ) return l2;
        if(l2 == null) return l1;

        if(l1.val < l2.val){
            l1.next =  mergeTwoListsRec(l1.next, l2);
            return l1;
        }else{
            l2.next =  mergeTwoListsRec(l1, l2.next);
            return l2;
        }
    }
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if (l1 == null && l2 == null) return null;

        ListNode head = new ListNode(0);
        ListNode tail = head;

        while (l1 != null && l2 != null) {
            ListNode left = l1;
            ListNode right = l2;
            if (l1.val <= l2.val) {
                tail.next = new ListNode(l1.val);
                tail = tail.next;
                l1 = l1.next;
            } else {
                tail.next = new ListNode(l2.val);
                tail = tail.next;
                l2 = l2.next;
            }
        }

        // append l2 values
        while (l1 != null) {
            tail.next = new ListNode(l1.val);
            tail = tail.next;
            l1 = l1.next;
        }
        // append l1 values
        while (l2 != null) {
            tail.next = new ListNode(l2.val);
            tail = tail.next;
            l2 = l2.next;
        }

        return head.next;

    }
}
