package leetcode.amazon.powerset;

import java.util.ArrayList;
import java.util.List;

public class PowerSet {

    public static List<List<Integer>> genPowerSet(List<Integer> set){
       return  helper(set, 0);

    }

    private static List<List<Integer>> helper(List<Integer> set, int index) {
        List<List<Integer>> allSubsets;
        if(set.size() == index){
            allSubsets = new ArrayList<>();
            allSubsets.add(new ArrayList<Integer>());
            return allSubsets;
        }
        allSubsets = helper(set, index+1);
        int item = set.get(index);
        List<List<Integer>> moresubsets = new ArrayList<>();
        for (List<Integer> subset: allSubsets) {
            List<Integer> newSubset = new ArrayList<>(subset);
            newSubset.add(item);
            moresubsets.add(newSubset);
        }
        allSubsets.addAll(moresubsets);

        return allSubsets;
    }

    public static ArrayList<ArrayList<Integer>> getSubsets(ArrayList<Integer> set, int index) {
        ArrayList<ArrayList<Integer>> allsubsets = null;
            System.out.println("set="+set.size() + " index="+index + ", set="+set + ", allsubsets="+allsubsets);
        if (set.size() == index) { // Base case - add empty set
            allsubsets = new ArrayList<ArrayList<Integer>>();
            allsubsets.add(new ArrayList<Integer>());
        } else {
            allsubsets = getSubsets(set, index + 1);
            int item = set.get(index);
            ArrayList<ArrayList<Integer>> moresubsets = new ArrayList<ArrayList<Integer>>();
            for (ArrayList<Integer> subset : allsubsets) {
                ArrayList<Integer> newsubset = new ArrayList<Integer>();
                newsubset.addAll(subset);
                newsubset.add(item);
                moresubsets.add(newsubset);
            }
            allsubsets.addAll(moresubsets);
        }
        System.out.println("END set="+set.size() + " index="+index + ", set="+set + ", allsubsets="+allsubsets);
        return allsubsets;
    }

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < 2; i++) {
            list.add(i);
        }
        List<List<Integer>> subsets = genPowerSet(list);
        System.out.println(subsets.toString());
    }
}
