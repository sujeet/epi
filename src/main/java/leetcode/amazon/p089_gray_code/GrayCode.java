package leetcode.amazon.p089_gray_code;

import java.util.ArrayList;
import java.util.List;

/**
 * 89. Gray Code
 * The gray code is a binary numeral system where two successive values differ in only one bit.
 *
 * Given a non-negative integer n representing the total number of bits in the code, print the sequence of gray code. A gray code sequence must begin with 0.
 *
 * Example 1:
 *
 * Input: 2
 * Output: [0,1,3,2]
 * Explanation:
 * 00 - 0
 * 01 - 1
 * 11 - 3
 * 10 - 2
 *
 * For a given n, a gray code sequence may not be uniquely defined.
 * For example, [0,2,3,1] is also a valid gray code sequence.
 *
 * 00 - 0
 * 10 - 2
 * 11 - 3
 * 01 - 1
 * Example 2:
 *
 * Input: 0
 * Output: [0]
 * Explanation: We define the gray code sequence to begin with 0.
 *              A gray code sequence of n has size = 2n, which for n = 0 the size is 20 = 1.
 *              Therefore, for n = 0 the gray code sequence is [0].
 */
public class GrayCode {
    /**
     * https://leetcode.com/problems/gray-code/discuss/155247/Recursive-solution-Java-Appending-0-and-1
     *
     * Base case n = 0**[0]**
     *
     * General case n = k. f(n) = [0 + f(n-1))] + [1 + reverse(f(n-1))]. Basically take all of the values of the previous solution and append 0 to it. (AKA doing nothing) and place it as the first elements in our solution array.
     *
     * Then take the previous solution again. Reverse it. Then append a 1 in the front. Then place the rest of the numbers in the array.
     *
     * Why this works.
     *
     * Informal Proof why works.
     *
     * Base case is trivial. Only one element in the array. Differs from everything else.
     *
     * For the general case assumming it works correctly for F(n-1) then we have a list of bits that differ by one. Then we will up the first part of list with these bits. All of these bits work correctly by inductive hypothesis. If we reverse the list, this does not change the fact the elements are still differ by one. But the middle elements will match if we leave the values as is. Therefore, we just append a one. Now the middle elements differ by one. The rest of the reversed list still maintains property of only differing by one since they all have one. Therefore, it does not modify the number of bits that they differ by (which is one). Therefore, each number in our solution vector differs by one.
     */
    public List<Integer> grayCode(int n) {
        if(n==0){
            List<Integer> res = new ArrayList<>();
            res.add(0);
            return res;
        }

        List<Integer> prev = grayCode(n-1);

        //Append a 0 to previous numbers and add it to the solution list (Do nothing and add it to the list)
        List<Integer> ans = new ArrayList<>(prev);

        //Reverse the list (backwards traversal of solution), Append a 1 to the front, Add to the list
        for (int i = prev.size()-1;  i>= 0; i--){
            int withoutOne = prev.get(i);
            int withOne = withoutOne | (1 << n-1);

            ans.add(withOne);
        }
        return ans;

    }
}
