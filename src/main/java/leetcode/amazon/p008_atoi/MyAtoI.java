package leetcode.amazon.p008_atoi;

import org.omg.PortableInterceptor.INACTIVE;

public class MyAtoI {

    public int myAtoi(String str) {
        if (str == null || str.isEmpty()) return 0;
        int res=0;
        int index = 0;
        boolean negative = false;
        int length = str.length();

        while(index < length -1 && str.charAt(index) == ' '){
            index++;
        }
        // index < length because of above condition
        char ch = str.charAt(index);
        if(ch == '+'){
            index++;
        }else if(ch == '-'){
            negative = true;
            index++;
        }

        for(; index < length; index++){
            ch = str.charAt(index);
            if(ch >= '0' && ch <= '9'){
                int num = ch -'0';
                boolean overflow = res > (Integer.MAX_VALUE - num) / 10;
                if(overflow){
                    return negative ? Integer.MIN_VALUE : Integer.MAX_VALUE;
                }
                res = res*10 + num ;
            }else{
                break;
            }
        }

        return negative? -res : res;

    }

    public int myAtoi_99(String str) {
        if (str == null || str.isEmpty()) return 0;
        boolean negative = false;
        int res = 0, index = 0;
        while (str.charAt(index) == ' ' && index < str.length() - 1) {
            index++;
        }
        if (str.charAt(index) == '-' || str.charAt(index) == '+') {
            if (str.charAt(index) == '-')
                negative = true;
            index++;
        }
        for (; index < str.length() && str.charAt(index) <= '9' && str.charAt(index) >= '0'; index++) {
            if (res <= (Integer.MAX_VALUE - str.charAt(index) + '0') / 10)
                res = res * 10 + str.charAt(index) - '0';
            else
                return negative ? Integer.MIN_VALUE : Integer.MAX_VALUE;
        }
        return negative ? -res : res;
    }

    public static void main(String[] args) {
        int max = Integer.MAX_VALUE;
        int min = Integer.MIN_VALUE;
        System.out.println("max =" + max); // 2147483647
        System.out.println("min =" + min); // -2147483648

        //  min = min * 10 - 8;
        //System.out.println("min =" + max);

        MyAtoI myAtoI = new MyAtoI();
        System.out.println(myAtoI.myAtoi("42"));
        System.out.println(myAtoI.myAtoi("   -42"));
        System.out.println(myAtoI.myAtoi("4193 with words"));
        System.out.println(myAtoI.myAtoi("words and 987"));
        System.out.println(myAtoI.myAtoi("-91283472332"));
        System.out.println(myAtoI.myAtoi("2147483648") +" it is max+1 ");
        System.out.println(myAtoI.myAtoi("2147483647") +" it is max ");
        System.out.println(myAtoI.myAtoi("2147483646") +" it is max-1 ");

        System.out.println(myAtoI.myAtoi("-2147483649") +" it is min-1 ");
        System.out.println(myAtoI.myAtoi("-2147483648") +" it is min ");
        System.out.println(myAtoI.myAtoi("-2147483647") +" it is min+1 ");
        System.out.println(myAtoI.myAtoi("-6147483648"));
        System.out.println(myAtoI.myAtoi("2147483646"));
        System.out.println(myAtoI.myAtoi_99("-91283472332"));
        System.out.println(myAtoI.myAtoi("-91283472332"));

    }
}
