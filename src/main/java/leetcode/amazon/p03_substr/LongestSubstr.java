package leetcode.amazon.p03_substr;

import java.util.HashMap;
import java.util.Map;

/**
 * Given a string, find the length of the longest substring without repeating characters.
 * <p>
 * Examples:
 * <p>
 * Given "abcabcbb", the answer is "abc", which the length is 3.
 * <p>
 * Given "bbbbb", the answer is "b", with the length of 1.
 * <p>
 * Given "pwwkew", the answer is "wke", with the length of 3. Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
 */
public class LongestSubstr {
    public int findLen2(String s) {
        int start = 0;
        int max = 0;
        int len = s.length();

        Map<Character, Integer> map = new HashMap<>();
        for (int idx = 0; idx < len; idx++) {
            char ch = s.charAt(idx);
            if (map.containsKey(ch)) {
                start = Math.max(map.get(ch) + 1, start);
            }
            map.put(ch, idx);
            max = Math.max(max, idx - start + 1);
        }
        return max;
    }

    public int lengthOfLongestSubstring3(String s) {
        HashMap<Character, Integer> map = new HashMap<>();
        int l = s.length();
        int res = 0;
        for (int idx = 0, start = 0; idx < l; idx++) {
            char ch = s.charAt(idx);
            if (map.containsKey(ch)) {
                start = Math.max(map.get(ch), start);
            }
            map.put(ch, idx + 1);
            res = Math.max(res, idx - start + 1);
        }

        return res;
    }

    public int findLen(String s){
        int result = 0;
        Map<Character, Integer> charInWindow = new HashMap<>();
        int left = 0;
        int right = 0;
        int len = s.length();
        while(right < len){
            char curChar = s.charAt(right);
            int charIdx = charInWindow.getOrDefault(curChar, -1);
            if(charIdx < left){
                // old repeat - ignore
                result = Math.max(result, right - left + 1);
            }else{
                // update left
                left = charIdx + 1;
            }
            charInWindow.put(curChar, right);
            right++;
        }
        return result;
    }
    public int lengthOfLongestSubstring4(String s) {
        int res = 0;
        Map<Character, Integer> charInWindow = new HashMap<>();
        int left = 0; // left boundary of the window
        int right = 0; // cur position we are exploring
        while (right < s.length()) {
            char curChar = s.charAt(right);
            int existedCharIdx = charInWindow.getOrDefault(curChar, -1);
            if (existedCharIdx < left) {
                // no duplicates in the window
                res = Math.max(res, right - left + 1);
            } else if (existedCharIdx >= left) {
                // duplicates found
                left = existedCharIdx + 1;
            }
            charInWindow.put(curChar, right);
            // move forward one step: add next new character into window
            right++;
        }
        return res;
    }
    public int lengthOfLongestSubstring2(String s) {
        if (s == null) return 0;

        char[] chars = s.toCharArray();
        int start = 0;
        int max = 0;
        int idx = 0;
        char ch;
        Map<Character, Integer> map = new HashMap<>();
        while (idx < chars.length) {
            ch = chars[idx];
            if (map.containsKey(ch)) {

                int len = idx - start;
                if (len > max) {
                    max = len;
                }
                // reset things
                idx = map.get(ch) + 1;
                start = idx;
                map.clear();
            } else {
                map.put(ch, idx);
                idx += 1;
            }
        }

        // check last
        if (max < idx - start) {
            max = idx - start;
        }
        return max;

    }

    public int lengthOfLongestSubstring(String s) {
        if (s == null) return 0;
        HashMap<Character, Integer> map = new HashMap<>();
        int len = s.length();
        int start = 0;
        int size = 0;
        for (int idx = 0; idx < len; idx++) {
            char ch = s.charAt(idx);
            if (map.containsKey(ch)) {
                start = Math.max(map.get(ch) + 1, start);
            }
            map.put(ch, idx);
            size = Math.max(size, idx - start + 1);
            System.out.println("Size=" + size + " at char " + ch + ", start=" + start + ", end=" + idx);
        }
        return size;
    }



    public static void main(String[] args) {
        LongestSubstr longestSubstr = new LongestSubstr();
        System.out.println(longestSubstr.findLen("abcabcbb") + "  needed=3 for abc");
        System.out.println(longestSubstr.findLen("bbb") + "  needed=1 for b");
        System.out.println(longestSubstr.findLen("pwwkew") + "  needed=3 for wke");
        System.out.println(longestSubstr.findLen("abcd") + "  needed=4 for abcd");
        System.out.println(longestSubstr.findLen("abba") + "  needed=2 for ab");
    }
}
