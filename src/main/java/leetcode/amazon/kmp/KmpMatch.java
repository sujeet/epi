package leetcode.amazon.kmp;

import java.util.Arrays;

public class KmpMatch {
    public int match(String input, String pattern) {
        if (input == null || input.isEmpty()) return -1;

        if (pattern == null || pattern.isEmpty()) return 0;

        int[] lps = createLps(pattern);

        char[]  patternArr = pattern.toCharArray();
        char[] inputArr = input.toCharArray();
        int patternIdx =0;
        int idx=0;
        while(idx < inputArr.length){

            if(patternArr[patternIdx] == inputArr[idx] ){
                patternIdx++;
                if(patternIdx >= patternArr.length){
                    // found full match
                    return idx - patternIdx +1;
                }
                idx++;
            }else{
                // not match
                if(patternIdx == 0){
                    idx++;
                }else{
                    patternIdx = lps[patternIdx -1];
                }
            }

        }

        return -1;
    }

    private int[] createLps(String pattern) {
        int[] lps = new int[pattern.length()];

        int lastMatch = 0;
        int curr = 1;

        while (curr < pattern.length()) {
            if (pattern.charAt(lastMatch) == pattern.charAt(curr)) {
                lps[curr] = lastMatch + 1;
                curr++;
                lastMatch++;
            } else {
                if (lastMatch > 0) {
                    lastMatch = lps[lastMatch - 1];
                }else{
                    lps[curr] = 0;
                    curr++;
                }

            }
        }

        return lps;
    }

    public static void main(String[] args) {
        KmpMatch kmp = new KmpMatch();

        int[] res1 = kmp.createLps("abcaby");
        System.out.println(Arrays.toString(res1) + " for abcaby");

        System.out.println(Arrays.toString(kmp.createLps("abcdabca")) + " for abcdabca");

        System.out.println(kmp.match("abcxabcdabxabcdabcdabcy", "abcdabca"));
    }
}
