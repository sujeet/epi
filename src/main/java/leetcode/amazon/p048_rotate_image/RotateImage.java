package leetcode.amazon.p048_rotate_image;

/**
 * You are given an n x n 2D matrix representing an image.
 * <p>
 * Rotate the image by 90 degrees (clockwise).
 * <p>
 * Note:
 * <p>
 * You have to rotate the image in-place, which means you have to modify the input 2D matrix directly. DO NOT allocate another 2D matrix and do the rotation.
 * <p>
 * Example 1:
 * <p>
 * Given input matrix =
 * [
 * [1,2,3],
 * [4,5,6],
 * [7,8,9]
 * ],
 * <p>
 * rotate the input matrix in-place such that it becomes:
 * [
 * [7,4,1],
 * [8,5,2],
 * [9,6,3]
 * ]
 * Example 2:
 * <p>
 * Given input matrix =
 * [
 * [ 5, 1, 9,11],
 * [ 2, 4, 8,10],
 * [13, 3, 6, 7],
 * [15,14,12,16]
 * ],
 * <p>
 * rotate the input matrix in-place such that it becomes:
 * [
 * [15,13, 2, 5],
 * [14, 3, 4, 1],
 * [12, 6, 8, 9],
 * [16, 7,10,11]
 * ]
 */
public class RotateImage {
    public void rotate(int[][] matrix) {
        if (matrix == null) return;

        int size = matrix.length;

        int layersToRotate = size / 2;

        // layer 0 is outer layer
        for (int layer = 0; layer < layersToRotate; layer++) {
            rotate(matrix, layer, size - 1);
        }

    }

    private void rotate(int[][] matrix, int layer, int n) {

        int offset = 0;
        for (int i = layer; i < n - layer; i++) {
            offset = i - layer;
            // Rotate top
            int top = matrix[layer][i];
            int right = matrix[layer + offset][n - layer];
            int bottom = matrix[n-layer][n-layer - offset];
            int left = matrix[n-layer-offset][layer];


            matrix[layer + offset][n - layer]= top;
            matrix[n-layer][n-layer - offset] = right;
            matrix[n-layer-offset][layer] = bottom;
            matrix[layer][i] = left;
        }
    }

    public static void main(String[] args) {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        print(matrix);
        System.out.println();
        RotateImage rotateImage = new RotateImage();
        rotateImage.rotate(matrix);
        print(matrix);


    }

    public static void print(int[][] matrix){
        for(int i=0; i< matrix.length; i++){
            for(int j=0; j< matrix.length; j++){
                System.out.print(" "+ matrix[i][j] + " ");
            }
            System.out.println();
        }
    }
}
