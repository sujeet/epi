package leetcode.amazon.p01_twosum;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * Two Sum
 * Problem
 * Given an array of integers, find two numbers such that they add up to a specific target number.
 * The function twoSum should return indices of the two numbers such that they add up to the target, where index1 must be less than index2. Please note that your returned answers (both index1 and index2) are NOT zero-based.
 * Example
 * numbers=[2, 7, 11, 15], target=9
 * return [1, 2]
 * Note
 * You may assume that each input would have exactly one solution
 * Challenge
 * Either of the following solutions are acceptable:
 * O(n) Space, O(nlogn) Time
 * O(n) Space, O(n) Time
 */
public class TwoSum {
    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();
       // int[] result = new int[2];
        for(int i=0; i< nums.length;  i++){
            int complement = target - nums[i];
            if(map.containsKey(complement)){
               // result[0] = map.get(nums[i]);
                //result[1] = i;
                return new int[] { map.get(complement), i};
            } else{
                map.put(nums[i], i);
            }

        }
        return null;
    }

    public static void main(String[] args) {
        int[] nums = {2,7,11,15};
        int target = 9;

        TwoSum twoSum = new TwoSum();
        int[] result = twoSum.twoSum(nums, target);

        System.out.println(Arrays.toString(result));
    }
}
