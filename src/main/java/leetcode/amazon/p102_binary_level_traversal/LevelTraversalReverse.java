package leetcode.amazon.p102_binary_level_traversal;

import leetcode.amazon.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * https://leetcode.com/problems/binary-tree-level-order-traversal/description/
 * 102. Binary Tree Level Order Traversal
 * Given a binary tree, return the level order traversal of its nodes' values. (ie, from left to right, level by level).
 * <p>
 * For example:
 * Given binary tree [3,9,20,null,null,15,7],
 * 3
 * / \
 * 9  20
 * /  \
 * 15   7
 * return its level order traversal as:
 * [
 * [3],
 * [9,20],
 * [15,7]
 * ]
 */
public class LevelTraversalReverse {
    public List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> results = new ArrayList<>();
        helper(root, 0, results);
        return results;

    }

    private void helper(TreeNode root, int level, List<List<Integer>> results) {
        if (root == null) return;

        if (level == results.size()) {
            List<Integer> res = new ArrayList<>();
            results.add(0, res);
        }

        helper(root.left, level + 1, results);
        helper(root.right, level + 1, results);
        results.get(results.size() - level - 1).add(root.val);


    }

    public static void main(String[] args) {
        /*
        *
        3
       / \
      9  20
        /  \
       15   7

        * */
        TreeNode root = new TreeNode(3);
        TreeNode t9 = new TreeNode(9);
        TreeNode t20 = new TreeNode(20);
        TreeNode t15 = new TreeNode(15);
        TreeNode t7 = new TreeNode(7);

        root.left = t9;
        root.right = t20;
        t20.left = t15;
        t20.right = t7;


        LevelTraversalReverse lt = new LevelTraversalReverse();
        List<List<Integer>> results = lt.levelOrder(root);
        System.out.println(results.toString());
    }
}
