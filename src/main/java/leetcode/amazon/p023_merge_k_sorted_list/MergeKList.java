package leetcode.amazon.p023_merge_k_sorted_list;

import leetcode.amazon.ListNode;

import java.util.List;

/**
 * Merge k sorted linked lists and return it as one sorted list. Analyze and describe its complexity.
 *
 * Example:
 *
 * Input:
 * [
 *   1->4->5,
 *   1->3->4,
 *   2->6
 * ]
 * Output: 1->1->2->3->4->4->5->6
 */
public class MergeKList {
    public ListNode mergeKLists(ListNode[] lists) {
        if (lists == null) return null;
        if(lists.length == 1) return lists[0];

        ListNode dummy = new ListNode(0);
        // it has more than one
        helper(lists, 0, dummy);
        return dummy.next;

    }

    private void helper(ListNode[] lists, int index, ListNode dummy) {
        if(index == lists.length){
            return;
        }
        dummy.next = merge(dummy.next, lists[index]);
        System.out.println("At index="+index);
        print(dummy);
        helper(lists, index+1, dummy);
    }

    public ListNode merge(ListNode left, ListNode right) {
       if(left == null) return right;
       if(right == null ) return left;

       if(left.val <= right.val){
           left.next = merge(left.next, right);
           return left;
       }else{
           right.next = merge(left, right.next);
           return right;
       }

    }

    public static void main(String[] args) {
        ListNode l1 = new ListNode(1);
        ListNode l1_4 = new ListNode(4);
        ListNode l1_4_5 = new ListNode(5);

        l1.next = l1_4;
        l1_4.next = l1_4_5;


        ListNode l2 = new ListNode(1);
        ListNode l2_3 = new ListNode(3);
        ListNode l2_3_4 = new ListNode(4);

        l2.next = l2_3;
        l2_3.next = l2_3_4;

        ListNode l3 = new ListNode(2);
        l3.next = new ListNode(6);



        MergeKList mergeKList = new MergeKList();

        System.out.println("l1=");
        mergeKList.print(l1);

        System.out.println("l2=");
        mergeKList.print(l2);

        System.out.println("l3=");
        mergeKList.print(l3);



        ListNode[] lists = {l1, l2,l3};
        ListNode res = mergeKList.mergeKLists(lists);
    //    ListNode res = mergeKList.merge(l1,l2);

        System.out.println("result= desired= 1->1->2->3->4->4->5->6" );
        mergeKList.print(res);


    }

    public void print(ListNode node){
        if(node == null){
            System.out.println();
            return;
        }

        System.out.print(node.val + "->");
        print(node.next);
    }

}
