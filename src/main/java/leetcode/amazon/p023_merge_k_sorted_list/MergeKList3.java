package leetcode.amazon.p023_merge_k_sorted_list;

import leetcode.amazon.ListNode;

import java.util.Comparator;
import java.util.PriorityQueue;

public class MergeKList3 {


    private ListNode mergeKLists(ListNode[] lists) {
        if (lists == null || lists.length == 0) return null;

        if (lists.length == 1) return lists[0];

        ListNode dummy = new ListNode(0);
        ListNode current = dummy;


        PriorityQueue<ListNode> queue = new PriorityQueue<>(new Comparator<ListNode>() {
            @Override
            public int compare(ListNode o1, ListNode o2) {
                return o1.val - o2.val;
            }
        });
        for (ListNode node : lists) {
            if (node != null) {
                queue.add(node);
            }
        }
        while (!queue.isEmpty()) {
            current.next = queue.remove();
            current = current.next;
            if (current.next != null) {
                queue.add(current.next);
            }
        }

        return dummy.next;
        // queue is empty

    }

    public static void main(String[] args) {
        ListNode l1 = new ListNode(1);
        ListNode l1_4 = new ListNode(4);
        ListNode l1_4_5 = new ListNode(5);

        l1.next = l1_4;
        l1_4.next = l1_4_5;


        ListNode l2 = new ListNode(1);
        ListNode l2_3 = new ListNode(3);
        ListNode l2_3_4 = new ListNode(4);

        l2.next = l2_3;
        l2_3.next = l2_3_4;

        ListNode l3 = new ListNode(2);
        l3.next = new ListNode(6);


        MergeKList3 mergeKList = new MergeKList3();

        System.out.println("l1=");
        mergeKList.print(l1);

        System.out.println("l2=");
        mergeKList.print(l2);

        System.out.println("l3=");
        mergeKList.print(l3);


        //     ListNode[] lists = {l1, l2,l3};
        ListNode[] lists = new ListNode[2];
        ListNode res = mergeKList.mergeKLists(lists);
        //    ListNode res = mergeKList.merge(l1,l2);

        System.out.println("result= desired= 1->1->2->3->4->4->5->6");
        mergeKList.print(res);


    }

    public void print(ListNode node) {
        if (node == null) {
            System.out.println();
            return;
        }

        System.out.print(node.val + "->");
        print(node.next);
    }

}
