package leetcode.amazon.p023_merge_k_sorted_list;

import leetcode.amazon.ListNode;

public class MergeKList2 {
    public ListNode mergeKLists(ListNode[] lists) {
        if (lists == null || lists.length == 0) return null;

        if (lists.length == 1) return lists[0];

        return mergeKLists(lists, 0, lists.length - 1);
    }

    private ListNode mergeKLists(ListNode[] lists, int left, int right) {
        if (left < right) {
            int mid = (left + right) /2;
            ListNode leftNode = mergeKLists(lists, left, mid);
            ListNode rightNode = mergeKLists(lists, mid+1, right);
            return  merge(leftNode, rightNode);

        }
        return lists[left];
    }

    private ListNode merge(ListNode leftNode, ListNode rightNode) {
        if(leftNode == null) return  rightNode;
        if(rightNode == null) return  leftNode;

        if(leftNode.val < rightNode.val){
            leftNode.next = merge(leftNode.next, rightNode);
            return leftNode;
        }else{
            rightNode.next = merge(leftNode, rightNode.next);
            return rightNode;
        }
    }
}
