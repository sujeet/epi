package leetcode.amazon.p023_merge_k_sorted_list;

import leetcode.amazon.ListNode;

/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public ListNode mergeKLists(ListNode[] lists) {
        return partition(lists,0,lists.length-1);
    }
    private ListNode partition(ListNode[] lists,int start, int end) {
        if(start == end) return lists[start];
        if(start<end) {
            int mid = start + (end - start)/2;
            ListNode l1 = partition(lists,start,mid);
            ListNode l2 = partition(lists,mid+1,end);
            return merge(l1,l2);
        } else {
            return null;
        }
    }
    private ListNode merge(ListNode l1, ListNode l2){
        if(l1==null) return l2;
        if(l2==null) return l1;
        ListNode result = null;
        if (l1.val <= l2.val) {
            result = l1;
            result.next = merge(l1.next, l2);
        } else {
            result = l2;
            result.next = merge(l1, l2.next);
        }
        return result;
    }
}