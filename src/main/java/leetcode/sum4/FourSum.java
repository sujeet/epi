package leetcode.sum4;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * 4Sum
 * Problem
 * Given an array S of n integers, are there elements a, b, c, and d in S such that a + b + c + d = target?
 *
 * Find all unique quadruplets in the array which gives the sum of target.
 *
 * Example
 * Given array S = {1 0 -1 0 -2 2}, and target = 0. A solution set is:
 *
 * (-1, 0, 0, 1)
 * (-2, -1, 1, 2)
 * (-2, 0, 0, 2)
 * Note
 * Elements in a quadruplet (a,b,c,d) must be in non-descending order. (ie, a ≤ b ≤ c ≤ d) The solution set must not contain duplicate quadruplets.
 */
public class FourSum {
    public ArrayList<ArrayList<Integer>> fourSum(int[] nums, int target) {
        Arrays.sort(nums);
        ArrayList<ArrayList<Integer>> results = new ArrayList<>();
        for(int i = 0; i < nums.length -3; i++){
            for(int j=i+1; j < nums.length -2; j++){
                int left = j+1;
                int right = nums.length -1;
                while(left < right){
                    int sum = nums[i] + nums[j] + nums[left] + nums[right];
                    if(sum == target){
                        ArrayList<Integer> tuple = new ArrayList<>(Arrays.asList(nums[i], nums[j], nums[left], nums[right]));
                        if(!results.contains(tuple))
                        results.add(tuple);
                        break;
                    }

                    if(sum < target){
                        left++;
                    }else{
                        right--;
                    }
                }
            }
        }
        return results;

    }

    public static void main(String[] args) {
        FourSum fourSum = new FourSum();
        int[] nums = {1, 0, -1, 0, -2, 2};
        int target = 0;
        ArrayList<ArrayList<Integer>> results = fourSum.fourSum(nums, target);
        System.out.println(results.toString());
    }
}
