package leetcode.binTree;

import java.util.*;

/**
 * Binary Tree Level Order Traversal
 * Problem
 * Given a binary tree, return the level order traversal of its nodes' values. (ie, from left to right, level by level).
 * Example
 * Given binary tree {3,9,20,#,#,15,7},
 * 3
 * / \
 * 9  20
 * /  \
 * 15   7
 * return its level order traversal as:
 * [
 * [3],
 * [9,20],
 * [15,7]
 * ]
 * Challenge
 * Challenge 1: Using only 1 queue to implement it.
 * Challenge 2: Use DFS algorithm to do it.
 *
 */
public class LevelTraversal2 {

    public List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> rst = new ArrayList<List<Integer>>();
        levelorder(root, rst, 0);
        return rst;
    }

    private void levelorder(TreeNode root, List<List<Integer>> rst, int level) {
        if (root == null) return;

        if (rst.size() == level) {
            rst.add(new ArrayList<>());
        }

        rst.get(level).add(root.val);
        levelorder(root.left, rst, level + 1);
        levelorder(root.right, rst, level + 1);
    }

    public List<List<Integer>> levelOrderRec(TreeNode root){
        List<List<Integer>> results = new ArrayList<>();
        levelOrderHelper(root, 0, results);
        return results;
    }

    private void levelOrderHelper(TreeNode node, int level, List<List<Integer>> results) {
        if(node == null) return;

        if(results.size()== level ){
            results.add(new ArrayList<>());
        }

        results.get(level).add(node.val);
        levelOrderHelper(node.left, level+1, results);
        levelOrderHelper(node.right, level+1, results);
    }

    public List<List<Integer>> levelOrderUsingDelimiter(TreeNode root) {
        List<List<Integer>> results = new ArrayList<>();
        Queue<TreeNode> queue = new LinkedList<>();

        queue.add(root);
        queue.add(null);
        TreeNode node;
        List<Integer> result = new ArrayList<>();
        while (!queue.isEmpty()) {
            node = queue.poll();
            if(node == null){
                results.add(result);
                result = new ArrayList<>();
                if(!queue.isEmpty()){
                    queue.offer(node);
                }
            }else{
                result.add(node.val);
                if(node.left != null){
                    queue.offer(node.left);
                }
                if(node.right != null){
                    queue.offer(node.right);
                }
            }
        }

        return results;
    }

    public List<List<Integer>> levelOrderUsingCount(TreeNode root) {
        List<List<Integer>> results = new ArrayList<>();
        Queue<TreeNode> queue = new LinkedList<>();

        int levelCount =1;
        int currentCount =0;
        queue.add(root);
        TreeNode node;
        List<Integer> result = new ArrayList<>();
        while (!queue.isEmpty()) {
            node = queue.poll();

            if(node.left != null){
                queue.offer(node.left);
                currentCount +=1;
            }
            if(node.right != null){
                queue.offer(node.right);
                currentCount +=1;
            }

            result.add(node.val);
            levelCount -=1;
            if(levelCount == 0){
                results.add(result);
                result = new ArrayList<>();
                levelCount = currentCount;
                currentCount = 0;
            }

        }

        return results;
    }

    public static void main(String[] args) {
        /*
        *
        3
       / \
      9  20
        /  \
       15   7

        * */
        TreeNode root = new TreeNode(3);
        TreeNode t9 = new TreeNode(9);
        TreeNode t20 = new TreeNode(20);
        TreeNode t15 = new TreeNode(15);
        TreeNode t7 = new TreeNode(7);

        root.left = t9;
        root.right = t20;
        t20.left = t15;
        t20.right = t7;


        LevelTraversal2 lt = new LevelTraversal2();
        List<List<Integer>> results = lt.levelOrderUsingDelimiter(root);
        List<List<Integer>> resultsCount = lt.levelOrderUsingCount(root);
        List<List<Integer>> resultsRec = lt.levelOrderRec(root);
        List<List<Integer>> resultsLo = lt.levelOrder(root);
        System.out.println(results.toString());
        System.out.println(resultsCount.toString());
        System.out.println(resultsRec.toString());
        System.out.println(resultsLo.toString());
    }

}
