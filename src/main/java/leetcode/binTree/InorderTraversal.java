package leetcode.binTree;

import java.util.*;


public class InorderTraversal {

    public List<Integer> inorderTraversalRec(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        inorderHelper(root, result);
        return result;
    }

    public List<Integer> inorderTraversal1(TreeNode root) {
        List<Integer> result = new ArrayList<>();

        Stack<TreeNode> stack = new Stack<>();
        pushLeftTree(root, stack);

        TreeNode node;
        while (!stack.isEmpty()){
            node = stack.pop();
            result.add(node.val);
            pushLeftTree(node.right,stack);
        }
        return result;

    }

    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<>();

        Deque<TreeNode> deque = new ArrayDeque<>();
        //pushLeftTree(root, stack);

     //   TreeNode node;
        while (root != null || !deque.isEmpty()){
            if(root == null){
                // reached he left end
                TreeNode node = deque.pollLast();
                root = node.right;
                result.add(node.val);
            }else{

                deque.add(root);
                root = root.left;
            }

        }
        return result;

    }

    /**
     * while (root != null || !st.isEmpty()) {
     *             if (root != null) {
     *                 st.addFirst(root);
     *                 root = root.left;
     *             } else {
     *                 root = st.removeFirst();
     *                 res.add(root.val);
     *                 root = root.right;
     *             }
     *         }
     *
     * @param node
     * @param stack
     */


    private void pushLeftTree(TreeNode node, Stack<TreeNode> stack) {
        while(node != null){
            stack.push(node);
            node = node.left;
        }
    }

    private void inorderHelper(TreeNode node, List<Integer> result) {
        if(node == null){
            return;
        }

        inorderHelper(node.left, result);
        result.add(node.val);
        inorderHelper(node.right, result);
    }

    public static void main(String[] args) {
        TreeNode root1 = new TreeNode(1);
        TreeNode tt2 = new TreeNode(2);
        TreeNode tt3 = new TreeNode(3);

        root1.right = tt2;
        tt2.left = tt3;

        InorderTraversal inorderTraversal = new InorderTraversal();
        List<Integer> result = inorderTraversal.inorderTraversal(root1);

        System.out.println(result.toString());

        TreeNode root = new TreeNode(10);

        TreeNode t1 = new TreeNode(1);
        TreeNode t11 = new TreeNode(11);
        TreeNode t6 = new TreeNode(6);
        TreeNode t12 = new TreeNode(12);

        root.left = t1;
        root.right = t11;
        t1.right = t6;
        t11.right = t12;

         result = inorderTraversal.inorderTraversal(root);

        System.out.println(result.toString());

        //[1, 6, 10, 11, 12]

        Deque<Integer> deque = new ArrayDeque<>();

        deque.addFirst(1);
        deque.addFirst(2);
        deque.addFirst(3);
        deque.add(900);
        deque.add(901);

        deque.push(200);

        System.out.println(deque);

    }
}
