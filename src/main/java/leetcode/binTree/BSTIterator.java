package leetcode.binTree;

import java.util.Stack;

public class BSTIterator {
    private  TreeNode root;
    private Stack<TreeNode> stack = new Stack<>();

    public BSTIterator(TreeNode root) {
        this.root = root;
        pushRightChildLeftTree(root);
    }

    // InOrder - l R r
    private boolean hasNext() {
        return !stack.isEmpty();
    }

    private TreeNode next() {
        TreeNode node = stack.pop();
        pushRightChildLeftTree(node.right);
        return node;
    }

    public void pushRightChildLeftTree(TreeNode node) {
            while(node != null){
                stack.push(node);
                node = node.left;
            }

    }




    public static void main(String[] args) {
        TreeNode root = new TreeNode(10);

        TreeNode t1 = new TreeNode(1);
        TreeNode t11 = new TreeNode(11);
        TreeNode t6 = new TreeNode(6);
        TreeNode t12 = new TreeNode(12);

        root.left = t1;
        root.right = t11;
        t1.right = t6;
        t11.right = t12;

        //[1, 6, 10, 11, 12]

        BSTIterator iterator = new BSTIterator(root);
        while (iterator.hasNext()) {
            TreeNode node = iterator.next();
            //*   do something for node
            System.out.print(node.val + "->");
        }
    }



}
