package leetcode.binTree;
import static java.lang.Math.max;

/**
 * https://leetcode.com/problems/binary-tree-maximum-path-sum/description/
 Given a non-empty binary tree, find the maximum path sum.

 For this problem, a path is defined as any sequence of nodes from some starting node to any node in the tree along the parent-child connections. The path must contain at least one node and does not need to go through the root.

 Example 1:

 Input: [1,2,3]

  1
 / \
 2   3

 Output: 6
 Example 2:

 Input: [-10,9,20,null,null,15,7]

 -10
 / \
 9  20
 /  \
 15   7

 Output: 42
 */
public class MaxPath {
    int max;
    public int maxPathSum(TreeNode root) {
        max = Integer.MIN_VALUE;
        longestPathToRoot(root);
        return max;
    }

    private int longestPathToRoot(TreeNode root) {
        // This function will return the max path to root node. As a side effect it also updates max seen till now
        if(root == null) return 0;

        // max dist from leaves to left node (root.left)
        int left = longestPathToRoot(root.left);

        // max dist from leaves to right node (root.right)
        int right = longestPathToRoot(root.right);

        if(left < 0 && right < 0){
            // exclude both dist as both are -ve
            max = Math.max(max, root.val);
        }else if(right < 0 || left < 0 ){
            // include the +ve one - here one is +ve and one is -ve
            max = Math.max(max,  root.val + Math.max(left, right));
        }else{
            // both are positive
            max = Math.max(max,  root.val + left + right);
        }

        // Max path to from left to root - note here we are not comparing with global max
        return Math.max(root.val, root.val + Math.max(left, right));
    }

    public int maxPath_old(TreeNode root) {
        if (root == null) return 0;

        int left = maxPath_old(root.left);
        int right = maxPath_old(root.right);
        max = Math.max(max, left + right + root.val);
        max = Math.max(max, root.val);
        return Math.max(left, right) + root.val;
    }



    public static void main(String[] args) {
        /**
         *   1
         *  / \
         *  2   3
         *
         *  Output: 6
         *  Example 2:
         *
         *  Input: [-10,9,20,null,null,15,7]
         *
         *  -10
         *  / \
         *  9  20
         *  /  \
         *  15   7
         *
         *  Output: 42
         */

        TreeNode r1 = new TreeNode(1);
        TreeNode r2 = new TreeNode(2);
        TreeNode r3 = new TreeNode(3);

        r1.left = r2;
        r1.right = r3;

        MaxPath maxPath = new MaxPath();

        System.out.println( maxPath.maxPathSum(r1));


    }
}
