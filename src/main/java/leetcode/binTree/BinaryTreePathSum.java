package leetcode.binTree;

import java.util.ArrayList;
import java.util.List;

/**
 * Print all k-sum paths in a binary tree
 * A binary tree and a number k are given. Print every path in the tree with sum of the nodes in the path as k.
 * A path can start from any node and end at any node and must be downward only, i.e. they need not be root node and leaf node; and negative numbers can also be there in the tree.
 * <p>
 * Examples:
 * <p>
 * Input : k = 5
 * Root of below binary tree:
 * 1
 * /     \
 * 3        -1
 * /   \     /   \
 * 2     1   4     5
 * /   / \     \
 * 1   1   2     6
 * <p>
 * Output :
 * 3 2
 * 3 1 1
 * 1 3 1
 * 4 1
 * 1 -1 4 1
 * -1 4 2
 * 5
 * 1 -1 5
 */
public class BinaryTreePathSum {
    public List<List<Integer>> binaryTreePathSum(TreeNode root, int target) {
        List<List<Integer>> results = new ArrayList<>();
        List<Integer> path = new ArrayList<>();
        helper(root, target, results, path);
        return results;
    }

    private void helper(TreeNode node, int target, List<List<Integer>> results, List<Integer> path) {
        if (node == null) return;

        //  System.out.println("target="+target + " path="+path + " node="+ node.val);
        int sum = path.stream().mapToInt(Integer::intValue).sum() + node.val;
        if (sum == target) {
            List<Integer> result = new ArrayList<>(path);
            result.add(node.val);
          //  if (!results.contains(result)) {
                results.add(result);
            //}else{
              //  System.out.println("Duplicate");
           // }
            return;

        }
        // ignore
        if(path.isEmpty()){
            helper(node.left, target, results, new ArrayList<>());
            path.add(node.val);
            helper(node.left, target, results, path);

            helper(node.right, target, results, new ArrayList<>());

            helper(node.right, target, results, path);
            path.remove(path.size() -1);
        }else{
            path.add(node.val);
            helper(node.left, target, results, path);
            helper(node.right, target, results, path);
            path.remove(path.size() -1);
        }





    }


    public static void main(String[] args) {
        /**
         *  * Input : k = 5
         *  *         Root of below binary tree:
         *  *            1
         *  *         /     \
         *  *       3        -1
         *  *     /   \     /   \
         *  *    2     1   4     5
         *  *         /   / \     \
         *  *        1   1   2     6
         *  *
         *  * Output :
         *  * 3 2
         *  * 3 1 1
         *  * 1 3 1
         *  * 4 1
         *  * 1 -1 4 1
         *  * -1 4 2
         *  * 5
         *  * 1 -1 5
         */
        int target = 5;
        TreeNode root = new TreeNode(1);

        TreeNode r3 = new TreeNode(3);
        TreeNode s1 = new TreeNode(-1);

        root.left = r3;
        root.right = s1;

        TreeNode r2 = new TreeNode(2);
        TreeNode r1 = new TreeNode(1);

        r3.left = r2;
        r3.right = r1;

        r1.left = new TreeNode(1);

        TreeNode r4 = new TreeNode(4);
        TreeNode r5 = new TreeNode(5);
        s1.left = r4;
        s1.right = r5;

        r4.left = new TreeNode(1);
        r4.right = new TreeNode(2);

        r5.right = new TreeNode(6);

        BinaryTreePathSum btps = new BinaryTreePathSum();

        List<List<Integer>> result = btps.binaryTreePathSum(root, target);
        System.out.println(result.toString());

    }
}
