package leetcode.binTree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Binary Tree Level Order Traversal II
 * Problem
 * Given a binary tree, return the bottom-up level order traversal of its nodes' values. (ie, from left to right, level by level from leaf to root).
 * Example
 * Given binary tree {3,9,20,#,#,15,7},
 *         3
 *        / \
 *       9  20
 *         /  \
 *        15   7
 * return its bottom-up level order traversal as:
 *     [
 *       [15,7],
 *       [9,20],
 *       [3]
 *     ]
 */
public class LevelTraversalReverse {

    public List<List<Integer>> levelOrderBottom(TreeNode root) {
        List<List<Integer>> wrapList = new LinkedList<List<Integer>>();
        levelMaker(wrapList, root, 0);
        return wrapList;
    }

    public void levelMaker(List<List<Integer>> list, TreeNode root, int level) {
        if(root == null) return;
        if(level >= list.size()) {
            list.add(0, new LinkedList<Integer>());
        }
        levelMaker(list, root.left, level+1);
        levelMaker(list, root.right, level+1);
        list.get(list.size()-level-1).add(root.val);
    }

    public List<List<Integer>> levelOrderBottom1(TreeNode root) {
        List<List<Integer>> results = new ArrayList<>();
        helper(root, 0, results);
        return results;
    }

    private void helper(TreeNode node, int level, List<List<Integer>> results) {
        if(node == null) return;

        if(results.size() == level){
            results.add(0,new ArrayList<>());
            System.out.println("results for level="+ level + " =="+ results.toString());
        }

        helper(node.left, level+1, results);
        helper(node.right, level+1, results);
        results.get(results.size() - level -1).add(node.val);


    }

    public static void main(String[] args) {
            /*
        *
        3
       / \
      9  20
        /  \
       15   7

        * */
        TreeNode root = new TreeNode(3);
        TreeNode t9 = new TreeNode(9);
        TreeNode t20 = new TreeNode(20);
        TreeNode t15 = new TreeNode(15);
        TreeNode t7 = new TreeNode(7);

        root.left = t9;
        root.right = t20;
        t20.left = t15;
        t20.right = t7;


        LevelTraversalReverse lt = new LevelTraversalReverse();
        List<List<Integer>> results = lt.levelOrderBottom(root);
        List<List<Integer>> results2 = lt.levelOrderBottom1(root);
        System.out.println(results.toString());
        System.out.println(results2.toString());
    }
}
