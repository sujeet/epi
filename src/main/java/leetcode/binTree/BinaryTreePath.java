package leetcode.binTree;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

/**
 * Given a binary tree, return all root-to-leaf paths.
 * <p>
 * Note: A leaf is a node with no children.
 * <p>
 * Example:
 * <p>
 * Input:
 * <p>
 * 1
 * /   \
 * 2     3
 * \
 * 5
 * <p>
 * Output: ["1->2->5", "1->3"]
 * <p>
 * Explanation: All root-to-leaf paths are: 1->2->5, 1->3
 */
public class BinaryTreePath {

    public List<String> binaryTreePaths(TreeNode root) {
        List<String> result = new ArrayList<>();


        Stack<Integer> st = new Stack<>();
        helper(root, st, result);
        return result;
    }

    public void helper(TreeNode root, Stack<Integer> prefix, List<String> result) {
        if (root.left == null && root.right == null) {
            // at leaf
            prefix.push(root.val);
            String path = prefix.stream().map(String::valueOf).collect(Collectors.joining("->"));
            result.add(path);
            prefix.pop();
            return;
        }
        prefix.push(root.val);
        if (root.left != null) {
            helper(root.left, prefix, result);

        }

        if (root.right != null) {
            helper(root.right, prefix , result);
        }
        prefix.pop();
    }

    public List<String> binaryTreePaths2(TreeNode root) {
        List<String> answer = new ArrayList<String>();
        if (root != null) searchBT(root, "", answer);
        return answer;
    }

    private void searchBT(TreeNode root, String path, List<String> answer) {
        if (root.left == null && root.right == null) answer.add(path + root.val);
        if (root.left != null) searchBT(root.left, path + root.val + "->", answer);
        if (root.right != null) searchBT(root.right, path + root.val + "->", answer);
    }


    public List<String> findPath(TreeNode root) {
        List<String> result = new ArrayList<>();
        helper1(root, "", result);
        return result;
    }

    private void helper1(TreeNode root, String prefix, List<String> result) {
        if(root == null) return;

        if(root.left == null && root.right == null){
            result.add(prefix + root.val);
        }
        if(root.left != null){
            helper1(root.left, prefix + root.val + "->" , result);
        }
        if(root.right != null){
            helper1(root.right, prefix + root.val + "->", result);
        }

    }


    public static void main(String[] args) {
        TreeNode r1 = new TreeNode(1);
        TreeNode r2 = new TreeNode(2);
        TreeNode r3 = new TreeNode(3);
        TreeNode r5 = new TreeNode(5);

        r1.left = r2;
        r1.right = r3;
        r2.right = r5;

        BinaryTreePath btp = new BinaryTreePath();
        System.out.println(btp.binaryTreePaths(r1));
        System.out.println(btp.findPath(r1));
    }
}
