package leetcode.binTree;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

/**
 * Binary Tree Level Order Traversal
 * Problem
 * Given a binary tree, return the level order traversal of its nodes' values. (ie, from left to right, level by level).
 * Example
 * Given binary tree {3,9,20,#,#,15,7},
 * 3
 * / \
 * 9  20
 * /  \
 * 15   7
 * return its level order traversal as:
 * [
 * [3],
 * [9,20],
 * [15,7]
 * ]
 * Challenge
 * Challenge 1: Using only 1 queue to implement it.
 * Challenge 2: Use DFS algorithm to do it.
 */
public class LevelTraversal {

    public List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> results = new ArrayList<>();
        Deque<TreeNode> left = new ArrayDeque<>();
        Deque<TreeNode> right = new ArrayDeque<>();

        left.add(root);
        Deque<TreeNode> dequeS;
        Deque<TreeNode> dequeD;
        while (!left.isEmpty() || !right.isEmpty()) {
            if (!left.isEmpty()) {
                dequeS = left;
                dequeD = right;
            } else {
                dequeS = right;
                dequeD = left;
            }

            List<Integer> result = new ArrayList<>();
            while (!dequeS.isEmpty()) {
                TreeNode node = dequeS.poll();
                result.add(node.val);
                if(node.left != null){
                    dequeD.add(node.left);
                }
                if(node.right != null){
                    dequeD.add(node.right);
                }
            }
            results.add(result);


        }

        return results;
    }

    public static void main(String[] args) {
        /*
        *
        3
       / \
      9  20
        /  \
       15   7

        * */
        TreeNode root = new TreeNode(3);
        TreeNode t9 = new TreeNode(9);
        TreeNode t20 = new TreeNode(20);
        TreeNode t15 = new TreeNode(15);
        TreeNode t7 = new TreeNode(7);

        root.left = t9;
        root.right = t20;
        t20.left = t15;
        t20.right = t7;


        LevelTraversal lt = new LevelTraversal();
        List<List<Integer>> results = lt.levelOrder(root);
        System.out.println(results.toString());
    }

}
