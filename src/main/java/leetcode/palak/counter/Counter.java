package leetcode.palak.counter;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicIntegerArray;

public class Counter {
    private static int GRANULARITY = 3600;
    private AtomicIntegerArray counter = new AtomicIntegerArray(GRANULARITY);
    private volatile int pos =0;

    private Counter() {
        PositionUpdator positionUpdator = new PositionUpdator(this);
        positionUpdator.start();
    }

  //  private static volatile RealTimeCounter INSTANCE;

    public void incrementPosition(){
        counter.set((pos+1) % GRANULARITY , 0);
        pos =(pos+1) % GRANULARITY;
    }

    public int hitCount(){
        int sum = 0;
        for (int i=0; i< GRANULARITY; i++ ) {
            sum = sum +  counter.get(i);
        }
        return sum;
    }
    public int hit(){
      return  counter.incrementAndGet(pos);
    }

    public static void main(String[] args) {
        System.out.println("HEllo");
    }
}


class PositionUpdator extends TimerTask{
    private final Counter counter;
    private final Timer timer = new Timer(true);

    private static final int DELAY = 1000;


    public PositionUpdator(Counter counter) {
        this.counter = counter;
    }

    public void start(){
        timer.schedule(this, DELAY);
    }

    // Run ever second

    @Override
    public void run() {
        counter.incrementPosition();
    }
}