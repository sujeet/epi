package leetcode.palak.ropeJoin;

import java.util.PriorityQueue;

public class RopeJoin {

    public static int findMinCost(int[] lengths){
        PriorityQueue<Integer> pq = new PriorityQueue<>();
        for(int l : lengths){
            pq.add(l);
        }

        while(pq.size() > 1){
            int left = pq.poll();
            int right = pq.poll();
            pq.add(left + right);
        }

        return pq.isEmpty() ? 0 : pq.poll();
    }

    public static void main(String[] args) {
        int len[] = {4, 3, 2, 6};
        int size = len.length;
        System.out.println("Total cost for connecting"+
                " ropes is " + findMinCost(len));
    }
}
