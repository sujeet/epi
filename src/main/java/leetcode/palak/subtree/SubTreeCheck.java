package leetcode.palak.subtree;

public class SubTreeCheck {

    public boolean isSubtree1(TreeNode s, TreeNode t) {
        if (t == null) return true;

        return subtree(s, t);

    }

    private boolean subtree(TreeNode s, TreeNode t) {
        if (s == null) return false;

        if (t.val == s.val) {
            if (match(s, t)) {
                return true;
            }
        }

        return subtree(s.left, t) || subtree(s.right, t);

    }

    private boolean match(TreeNode s, TreeNode t) {
        if(t == null) return true;
        if(s == null) return false;

        if(s.val != t.val){
            return false;
        }
        return match(s.left, t.left) && match(s.right, t.right);
    }

    public boolean isSubtree(TreeNode s, TreeNode t) {
        String tree1 = preorder(s, true);
        String tree2 = preorder(t, true);

        return tree1.contains(tree2) || tree2.contains(tree1);
    }


    private String preorder(TreeNode s, boolean left) {
        if (s == null) {
            if (left) {
                return "lnull";
            } else {
                return "rnull;";
            }
        }
        return "#" + s.right + " " + preorder(s.left, true) + " " + preorder(s.right, true);
    }
}
