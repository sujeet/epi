package ch5_primitive_type;

import java.util.HashMap;
import java.util.Map;

public class ConcEx {
    public static void main(String[] args) {
        Map<String, Integer> score = new HashMap<>();

        score.put("s1",1);
        score.put("s2",2);
        score.put("s3",2);
        score.put("s4",2);
        score.put("s5",2);
        score.put("s6",2);

        for ( Map.Entry<String, Integer> entry : score.entrySet()) {

            System.out.println(entry.getValue());
            score.put("s7", 7);
        }
    }
}
