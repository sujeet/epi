package ch5_primitive_type;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ConcHmap {
    static Map<String, Integer> orders = new ConcurrentHashMap<>();

    public static void main(String[] args) throws InterruptedException {

        orders.put("Mumbai", 0);
        orders.put("Beijing", 0);
        orders.put("Ny", 0);
        orders.put("London", 0);

        ExecutorService executorService = Executors.newFixedThreadPool(2);

        executorService.submit(ConcHmap::processOrders);
        executorService.submit(ConcHmap::processOrders);
        executorService.shutdown();

        executorService.awaitTermination(1, TimeUnit.SECONDS);

        System.out.println(orders);

    }

    private static void processOrders() {
        for (String city : orders.keySet()){
            for(int i=0; i<50; i++){
                Integer amount = orders.get(city);
                orders.put(city, amount+1);
            }
        }
    }
}
