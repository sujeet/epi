package ch5_primitive_type;

public class CountBits {

    public static short count(int x) {
        short numBits = 0;
        while (x != 0) {
            numBits += (x & 1);
            System.out.println("x = " + x + " binary= "+ Integer.toBinaryString(x) + " , numBits="+numBits);
            x = x >>> 1;
        }
        return numBits;
    }

    public static short parity(long x) {
        short result = 0;
        while (x != 0) {
            result ^= 1;
            System.out.println("x = " + x + " binary= "+ Long.toBinaryString(x) + " , result="+result);
            x &= (x-1);
        }
        return result;
    }

    public static short test(long x) {
        short result = 0;

        while (x != 0) {
            result += 1;
            System.out.println("x = " + x + " binary= "+ Long.toBinaryString(x) + " , result="+result);
            x &= ~(x-1);
            if(result == 5) break;
        }
        return result;
    }



    public static void main(String[] args) {
       // System.out.println(parity(197796789789L));
       // System.out.println(test(Long.valueOf("11000011000", 2)));
        /*System.out.println(Long.toBinaryString(1560));
        System.out.println(Long.toBinaryString(1559));
        System.out.println(Long.toBinaryString(~1559));
        System.out.println(Long.toBinaryString(1560 & ~1559));*/

       // Long l1 = Long.valueOf(1 << 6);
        //System.out.println(Long.toBinaryString(l1));

        int input = Integer.valueOf("01001001", 2);
        int i=1;
        int j=5;

        int bitMask = (1 << i)|(1 << j);
        System.out.println( "in ="+ Integer.toBinaryString(input));
        System.out.println( "bitmask "+ Integer.toBinaryString(bitMask));
        int out = input ^ bitMask;
        System.out.println("out="+Integer.toBinaryString(out));


    }
}
