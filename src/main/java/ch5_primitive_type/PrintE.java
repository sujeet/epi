package ch5_primitive_type;

import java.util.Scanner;

public class PrintE {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
       /* int N = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        if (N % 2 == 1) {
            System.out.println("Wierd");
        } else {
            // even
            if (N == 2 || N == 4 || N > 20) {
                System.out.println("Not Wierd");
            } else {

                System.out.println("Wierd");
            }

        }

        scanner.close();*/
        String s1 = "Hello";
        int x = 12;
        System.out.printf("%-15s%03d\n",s1,x);

        String s2 = "lo";
        int x2 = 3;
        System.out.printf("%-15s%03d\n",s2,x2);
    }
}
