package ch5_primitive_type;

import java.util.Arrays;

public class QuickSort {
    // 10

    public static void sort(int[] input){
        helper(input, 0, input.length);
    }

    public static void helper(int[] input, int start, int end){
        if(end <= start) return;

        int pivot = input[end-1];
        int leftIdx = start;
        int tmp;
        for (int idx=start; idx < end -1; idx++) {
            if(input[idx] < pivot){
                // swap
                tmp = input[leftIdx];
                input[leftIdx] = input[idx];
                input[idx] = tmp;
                // increase idx
                leftIdx++;
            }
        }
        input[end-1] = input[leftIdx];
        input[leftIdx] = pivot;
        helper(input, start, leftIdx);
        helper(input, leftIdx +1, end);
    }

    public static void main(String[] args) {
        int[] arr = {8,23,47,3,56,23,86,34,23,7,98};

        sort(arr);
        System.out.println(Arrays.toString(arr));
    }
}
