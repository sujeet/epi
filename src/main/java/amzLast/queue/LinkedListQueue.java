package amzLast.queue;

/**
 * Implementing Queue using arrays, LinkedLists & stacks.
 */
public class LinkedListQueue {
    ListNode head;
    ListNode tail;
    int size;
    public LinkedListQueue(){
        this.size = 0;
    }

    public boolean isEmpty(){
        return size == 0;
    }

    public void enqueue(int val){
        System.out.println("ENq "+ val + ", l="+size);
        ListNode node = new ListNode(val);
        if(tail == null){
            tail = node;
            head = node;
        }else{
            tail.next = node;
            tail = tail.next;
        }
        size++;

    }

    public Integer dequeue(){
        if(isEmpty()){
            System.out.println("DEq empty i="  +size);
            return null;
        }
        int val = head.val;
        head = head.next;
        size--;
        System.out.println("DEq val "+ val + ",  h="+ size);
        if(head == null){
            tail = null;
        }
        return val;
    }

    public static void main(String[] args) throws Exception {
        LinkedListQueue q = new LinkedListQueue();
        q.enqueue(1);
        q.enqueue(2);
        q.dequeue();
        q.dequeue();
        System.out.println(q.dequeue());
        System.out.println(q.dequeue());
        q.enqueue(6);
        q.enqueue(7);
        System.out.println(q.dequeue());
        System.out.println(q.dequeue());
        System.out.println(q.dequeue());
        System.out.println(q.dequeue());
    }
}
