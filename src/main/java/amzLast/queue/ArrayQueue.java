package amzLast.queue;

import java.util.Arrays;

/**
 * Implementing Queue using arrays, LinkedLists & stacks.
 */
public class ArrayQueue {
    private int[] arr;
    private int capacity;
    private int size;
    private int head;
    private int tail;

    public ArrayQueue(int capacity){
        this.capacity = capacity;
        this.head = this.size = 0;
        this.tail = capacity -1;
        arr = new int[capacity];
    }

    public boolean isFull(){
        return (this.size == capacity);
    }

    public boolean isEmpty(){
        return (this.size == 0);
    }

    public int size(){
        return size;
    }
    public int getNextPosition(int pos){
        return  (pos + 1) % capacity;
    }
    public boolean enqueue(int val){
        if(isFull()){
            return  false;
        }
        tail = getNextPosition(tail);
        arr[tail] = val;
        size++;
        System.out.println("Enq" + val + ", Array="+ Arrays.toString(arr));
        return true;
    }

    public Integer dequeue(){
        if(isEmpty()){
            return null;
        }
        int val = arr[head];
        head = getNextPosition(head);
        size--;
        System.out.println("Deq" + val + ", Array="+ Arrays.toString(arr));
        return val;
    }
    public static void main(String[] args) throws Exception {
        ArrayQueue q = new ArrayQueue(3);
        q.enqueue(1);
        q.enqueue(2);
        q.enqueue(3);
        q.enqueue(4);
        System.out.println(q.dequeue());
        System.out.println(q.dequeue());
        q.enqueue(6);
        q.enqueue(7);
        System.out.println(q.dequeue());
        System.out.println(q.dequeue());
        System.out.println(q.dequeue());
        System.out.println(q.dequeue());
    }

}
