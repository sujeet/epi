package amzLast.queue;

public class Stack {
    ListNode tail;

    public boolean isEmpty(){
        if(tail == null){
            return true;
        }
        return false;
    }

    public void push(int val) {
        ListNode node = new ListNode(val);
        if (tail == null) {
            tail = node;
        } else {
            node.next = tail;
            tail = node;
        }
    }

    public Integer pop(){
        if(tail == null){
            return null;
        }
        ListNode node = tail;
        tail = tail.next;
        return node.val;
    }

    public static void main(String[] args) {
        Stack st = new Stack();
        st.push(1);
        st.push(2);
        System.out.println(st.pop());
        System.out.println(st.pop());
        System.out.println(st.pop());
        System.out.println(st.pop());
        st.push(5);
        st.push(6);
        st.push(7);
        System.out.println(st.pop());
        System.out.println(st.pop());
        System.out.println(st.pop());
        System.out.println(st.pop());
    }
}
