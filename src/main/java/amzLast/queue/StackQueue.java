package amzLast.queue;

public class StackQueue {
    Stack in;
    Stack out;
    public StackQueue(){
        in = new Stack();
        out = new Stack();
    }


    public void enqueue(int val) {
        in.push(val);
    }

    public Integer dequeue(){
        if(out.isEmpty()){
            // copy values from in
            copy();
            if(!out.isEmpty()){
                return out.pop();
            }else{
                return null;
            }
        }

        return out.pop();
    }

    public void copy(){
        while(!in.isEmpty()){
            out.push(in.pop());
        }
    }


    public static void main(String[] args) throws Exception {
        StackQueue q = new StackQueue();
        q.enqueue(1);
        q.enqueue(2);
        System.out.println(q.dequeue());
        System.out.println(q.dequeue());
        System.out.println(q.dequeue());
        q.enqueue(6);
        q.enqueue(7);
        System.out.println(q.dequeue());
        System.out.println(q.dequeue());
        System.out.println(q.dequeue());
        System.out.println(q.dequeue());
    }

}
