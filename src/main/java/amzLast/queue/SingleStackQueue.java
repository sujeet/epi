package amzLast.queue;

import java.util.Stack;

public class SingleStackQueue {
    java.util.Stack<Integer> in;
    public SingleStackQueue(){
        in = new Stack<>();
    }


    public void enqueue(int val) {
        in.push(val);
    }

    public Integer dequeue(){
        if(in.isEmpty()){
            return null;
        }
        return dqHelper(in);

    }

    public Integer dqHelper(Stack<Integer> st){
        if(in.size() == 1){
            return in.pop();
        }
        int val = in.pop();
        int res = dqHelper(st);
        st.push(val);
        return res;
    }




    public static void main(String[] args) throws Exception {
        SingleStackQueue q = new SingleStackQueue();
        q.enqueue(1);
        q.enqueue(2);
        System.out.println(q.dequeue());
        System.out.println(q.dequeue());
        System.out.println(q.dequeue());
        q.enqueue(6);
        q.enqueue(7);
        System.out.println(q.dequeue());
        System.out.println(q.dequeue());
        System.out.println(q.dequeue());
        System.out.println(q.dequeue());
    }

}
